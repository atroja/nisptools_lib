import numpy as np
import numpy as np
from astropy.io import fits
import os
from nisptools.detectors import Detectors
from nisptools.io import read_focalplane_from_fits, read_acquisition, write_focalplane_to_fits, read_acquisition_fnames

#*****************************
#                            *
#    NO OFFSET NO SCALING    *
#                            *
#*****************************


def Darks_sum_FP (dirpath, fname1_array, badpix, outfname_stem, NPIXX = 2048, NPIXY = 2048, science_header = 'SCI{}{}', chi2_header = 'CHI2{}{}', selectarea = None, chi2dist = False, chi2_threshold = 47):
    
    print ("Darks_sum START")

    dets = Detectors ()
        
    badpix_pos = []
    for n in range (16):
        badpix_pos.append (np.where (badpix [n] == 1))
    
    Ex       = np.zeros ([1, 16, NPIXX, NPIXY], dtype = int)
    Ex2      = np.zeros ([1, 16, NPIXX, NPIXY], dtype = int)
    pixcount = np.zeros ([1, 16, NPIXX, NPIXY], dtype = int)
    count    = np.zeros ([1, 16], dtype = int) 
    if chi2dist:
        Exchi2 = np.zeros ([1, 16, NPIXX, NPIXY], dtype = int)

    
    area     = np.zeros ([NPIXX, NPIXY], dtype = bool)
    if selectarea:
        if len(selectarea) == 4:
            area [selectarea[0]:selectarea[1], selectarea[2]:selectarea[3]] = True
        if len(selectarea) == 2:
            area [:selectarea[0], :] = True
            area [-selectarea[0]:, :] = True
            area [:, :selectarea[1]] = True
            area [:, -selectarea[1]:] = True
    else:
        area [:,:] = True
        
    count_map = np.empty ([NPIXX,NPIXY], dtype = int)    
    
    n = 0
    for fname in fname1_array:   
        
        print (n+1, "of", len (fname1_array))
        
        science = read_focalplane_from_fits (dirpath+fname, NPIXX = NPIXX, NPIXY = NPIXY, mode = [science_header], data_type = int)
        chi2 = read_focalplane_from_fits (dirpath+fname, NPIXX = NPIXX, NPIXY = NPIXY, mode = [chi2_header], data_type = int)
 
        for d1 in range (1,5):
            for d2 in range (1,5):
                det = dets.det2indx ['{:2d}'.format(10*d1+d2)] - 1
        
                saturation_pos = np.where (science [det] == 65535)
                if np.max (chi2[det]) > 1:
                    chi2high_pos = np.where (chi2[det] > chi2_threshold)
                else:
                    chi2high_pos = np.where (chi2[det] > 0.9)
                
                chi2_over = len (chi2high_pos[0])
                if chi2dist:
                    Exchi2 [0][det]  = Exchi2 [0][det] + chi2 [det]
                
                count_map.fill (1.)
                count_map [saturation_pos[0], saturation_pos[1]] = 0.
                count_map [chi2high_pos[0], chi2high_pos[1]] = 0.
                count_map [badpix_pos [det][0], badpix_pos [det][1]] = 0.
            
                science[det] = science[det] * count_map
    
                pixcount [0][det] = pixcount [0][det] + count_map
                Ex [0][det]  = Ex [0][det]  + science [det]
                Ex2 [0][det] = Ex2 [0][det] + science[det]**2
            
                count [0][det] += 1
                
        n += 1
                
    print ("Writing {}".format (outfname_stem + "_PixCount.fits"))
    write_focalplane_to_fits (outfname_stem + "_PixCount.fits", pixcount, mode = [science_header])
    print ("Done")
               
    print ("Writing {}".format (outfname_stem + "_Ex.fits"))
    write_focalplane_to_fits (outfname_stem + "_Ex.fits", Ex, mode = [science_header] )
    print ("Done")
               
    print ("Writing {}".format (outfname_stem + "_Ex2.fits"))
    write_focalplane_to_fits (outfname_stem + "_Ex2.fits", Ex2, mode = [science_header])
    print ("Done")
    
    if chi2dist:
        print ("Writing {}".format (outfname_stem + "_ExCHI2.fits"))
        write_focalplane_to_fits (outfname_stem + "_CHI2AV.fits", Exchi2, mode = [science_header])
        print ("Done")

    print ("Darks_sum DONE")