import numpy as np
import os
import numpy as np
from astropy.io import fits
from nisptools.detectors import Detectors

def read_badpixel (arg_fname_dir, NPIXX=2048, NPIXY = 2048):
    
    det = Detectors ()
    
    fname_badpix = np.array (os.listdir(arg_fname_dir), dtype = '<U100')
    fname_mask = np.zeros (len(fname_badpix), dtype = bool)
    for i in range(len(fname_badpix)):
        if ".MAPS" in fname_badpix [i]:
            fname_mask [i] = True

    fname_badpix = np.array(fname_badpix [fname_mask])
    
    badpix = np.empty([16,NPIXX,NPIXY], dtype=int)
    
    for k in fname_badpix:
        try:
            n = det.det2indx [det.inv_det_trip [k[:3]]]
        except:
            continue
            
        tmp = fits.open (arg_fname_dir + k)
        if int(det.det_trip[det.indx2det[n]]) != tmp[0].header['SCS_ID']:
            print ('ERROR', det.det_trip[det.indx2det[n]], tmp[0].header['SCS_ID'])
            break 
        badpix [n-1] = tmp[1].data.astype(int)
        
    return badpix

def read_acquisition_fnames (arg_fname_dir, iws_files = True, first_fname_id = 1, last_fname_id = 10000, pid = [-1], acq_type = None, MACC = None):
    arg1 = os.path.split(arg_fname_dir)
    dirpath = arg1 [0]
    fname_stem = arg1 [1]
    print ('Reading header in {}* files inside {} folder'.format (fname_stem, dirpath))
    
    fname_array = np.array (os.listdir(dirpath), dtype = '<U100')
    fname_array = np.sort (fname_array)
    
    fname_mask = np.zeros (len(fname_array), dtype = bool)
    for i in range  (len(fname_array)):
        if fname_stem in fname_array [i]:
            fname_mask [i] = True
    
    fname_array = fname_array [fname_mask]
    
    if iws_files:
        #fname_mask = np.zeros (len(fname_array), dtype = bool)
        for i in range(len(fname_array)):
            try:
                if int(fname_array [i][-15:-11]) < first_fname_id:
                    continue
                if int(fname_array [i][-15:-11]) > last_fname_id:
                    continue
            except ValueError:
                continue
            #if fname_stem in fname_array [i]:
            #    fname_mask [i] = True
    
        fname_array = fname_array [fname_mask]

    if acq_type:
        n = 0
        type_mask = np.zeros (len(fname_array), dtype = bool)
        for i in fname_array:
            try:
                tmp = fits.open (dirpath+'/'+i)
                acq_type_in = tmp.info (False) [0][1]
                tmp.close ()
            except:
                continue
            if acq_type_in == acq_type:
                type_mask [n] = True
            n += 1
        fname_array = fname_array [type_mask]

    if pid [0] != -1:
        n = 0
        pid_mask = np.zeros (len(fname_array), dtype = bool)
        for i in fname_array:
            try:
                tmp = fits.open (dirpath+'/'+i)
                pid_in = (tmp[0].header ['PID2'] + tmp[0].header ['PID1'] * 65536)
                tmp.close ()
            except:
                continue
            if pid_in in pid:
                pid_mask [n] = True
            n += 1
        fname_array = fname_array [pid_mask]    
        
    if MACC:
        n = 0
        macc_mask = np.zeros (len(fname_array), dtype = bool)
        for i in fname_array:
            try:
                tmp = fits.open (dirpath+'/'+i)
                groups = tmp[0].header['GRP_CNT']
                frames = tmp[0].header['FRAMEGRP']
                drops = tmp[0].header['DROPS']
                tmp.close ()
            except:
                continue
            if np.array_equal(MACC, np.array([groups, frames, drops])):
                macc_mask [n] = True
            n += 1
        fname_array = fname_array [macc_mask]
            
    return fname_array

def read_acquisition (arg_fname_dir):
    
    det = Detectors ()

    # ORDER: fname, time, det, mod, macc, pid, mean, variance, median
    headerin = []

    tmp = fits.open (arg_fname_dir)
        
    headerin.append(os.path.split(arg_fname_dir)[1])
    headerin.append(  tmp[0].header ['DATE-OBS'])
    headerin.append(  tmp[0].header ['SCE_POS'])
    headerin.append(  tmp[0].header ['EXTNAME'])
    
    #headerin.append([ tmp[0].header ['GRP_CNT'],
    #                  tmp[0].header ['FRAMEGRP'],
    #                  tmp[0].header ['DROPS']])
    headerin.append(  tmp[0].header ['GRP_CNT'])
    headerin.append(  tmp[0].header ['FRAMEGRP'])
    headerin.append(  tmp[0].header ['DROPS'])
    
    headerin.append ( tmp[0].header ['PID2']+65536*tmp[0].header['PID1'])

    science = tmp ['SCIENCE'].data.astype (int)
    chi2 = tmp ['CHI2'].data.astype (int)
        
    tmp.close ()
    
    return headerin, science, chi2
    
def read_focalplane_from_fits (fname, NPIXX = 2048, NPIXY = 2048, mode = ['PHOTO{:1d}{:1d}', 'SPECTRO{:1d}{:1d}'], data_type=float):
    det = Detectors ()
    
    avv_in = fits.open (fname)

    nmods = len (mode)
    if nmods > 1:
        avv = np.zeros ([nmods,16, NPIXX, NPIXY])
        for mod in range (nmods):
            for i in range (1,5):
                for j in range  (1,5):
                    #avv [mod][det.det2indx ['{}{}'.format (i,j)] - 1] = avv_in[mode[mod] + '{}{}'.format (i,j)].data
                    avv [mod][det.det2indx ['{}{}'.format (i,j)] - 1] = avv_in[mode[mod].format (i,j)].data.astype (data_type)

    else:
        avv = np.zeros ([16, NPIXX, NPIXY])
        for i in range (1,5):
            for j in range  (1,5):
                #avv[det.det2indx ['{}{}'.format (i,j)] - 1] = avv_in[mode[0] + '{}{}'.format (i,j)].data
                avv[det.det2indx ['{}{}'.format (i,j)] - 1] = avv_in[mode[0].format (i,j)].data.astype (data_type)
    return avv

def write_focalplane_to_fits (fname, frame, mode = ['PHOTO{:1d}{:1d}', 'SPECTRO{:1d}{:1d}']):
    det = Detectors ()
    hdu = []
    hdu.append (fits.PrimaryHDU())
    nmods = len (mode)
    
    if nmods > 1:
        for f in range (nmods):
            for n in range (16):
                hdu.append (fits.ImageHDU(frame[f][n], name = mode[f].format(int(det.indx2det[n+1][0]),
                                                                             int(det.indx2det[n+1][1]))))
    else:
        for n in range (16):
            hdu.append (fits.ImageHDU(frame[0][n], name = mode[0].format(int(det.indx2det[n+1][0]),
                                                                      int(det.indx2det[n+1][1]))))
        

    hdul = fits.HDUList(hdu)
    hdul.writeto(fname)
