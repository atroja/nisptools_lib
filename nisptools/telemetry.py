"""
Created on May 2021

@author: Louis Gabarra 

"""

############################

"""
We define in this script some functions to be used for the search of potential PSF seed pixels as well as functions to link Images to the relative House Keeping telemetry parameters.

"""

import numpy as np
import matplotlib.pyplot as plt
import fnmatch
import matplotlib.backends.backend_pdf
import scipy.optimize as opt
import pandas as pd
from mpl_toolkits import mplot3d
from matplotlib.ticker import AutoMinorLocator, FormatStrFormatter
from matplotlib.ticker import FixedLocator, FixedFormatter
import time
from datetime import date
from astropy.io import fits
from scipy.stats import gaussian_kde
from datetime import datetime
from scipy.optimize import curve_fit
import os
import re
import math

def get_bad_pixels_SCI(path_bad_pixels):
    DET_IDs      = ['453-024-019.MAPS'
                    ,'272-042-024.MAPS','632-085-017.MAPS'
                    ,'267-084-043.MAPS','268-045-019.MAPS','285-064-024.MAPS'
                    ,'548-071-025.MAPS', '452-054-043.MAPS','280-114-056.MAPS'
                    ,'284-081-017.MAPS','278-015-043.MAPS','269-072-056.MAPS'
                    ,'458-103-025.MAPS','249-014-015.MAPS', '221-094-056.MAPS'
                    ,'628-052-025.MAPS']
    
    BAD_PIX_MAPS = np.zeros((16,2040,2040))
    
    for count, DET_ID in enumerate(DET_IDs):
        BAD_PIX_MAP              = fits.open(path_bad_pixels+DET_ID)
        BAD_PIX_MAPS[count,:,:]  = BAD_PIX_MAP['dead_pixels'].data[4:-4,4:-4].astype(int)
        
    return BAD_PIX_MAPS

def get_Image_SCI(path_dark):
    IMAGE                   = fits.open(path_dark)
    IMAGE_SCI               = IMAGE['SCIENCE'].data.astype(int)
    return IMAGE_SCI

def get_param_from_fits_file(path_files_IM1, path_files_IM2, path_files_IM3, file_name):
    #HDUL                    = fits.open((path_files + str(str(file_name)[1:-1])[1:-1]).replace("'",""))#str(file_name)[1:-1])
    try:
        HDUL                    = fits.open(path_files_IM1 + file_name)
    except:
        try:
            HDUL                    = fits.open(path_files_IM2 + file_name) 
        except:
            try:
                HDUL                    = fits.open(path_files_IM3 + file_name)
            except:
                print('Problem found while opening the fits files of the images')
                
    SCI_ALL                 = HDUL['SCIENCE'].data.astype(int)
    
    try:
        CHI2_ALL                = HDUL['CHI2'].data.astype(int)
    except:
        print('Error CHI2 is missing for the file ',path_files,file_name)
        
    try:
        PID                     = HDUL[0].header['PID1'] * 65536 + HDUL[0].header['PID2']
    except:
        print('Error PID missing for file',path_files,file_name)
        
    try:
        DET_ID                  = int(HDUL[0].header['SCE_POS'])    
    except:
        print('Error DET_ID missing for the file',path_files,file_name)
       
    return HDUL,SCI_ALL,CHI2_ALL,PID,DET_ID

def get_param_from_fits_file_BIS(HDUL, INFO_WANTED_FROM_IMAGES):
    
    try:
        TSTART                  = HDUL[0].header['TSTART']
        DATE_OBS_sec_CET        = time.mktime(datetime.strptime(HDUL[0].header['DATE-OBS'], '%Y-%m-%dT%H:%M:%S').timetuple())  
    except:
        print('Error DATE OBS parameters missing for file',path_files,file_name)
        
    try:
        MACCGRP                 = HDUL[0].header['GRP_CNT']
        MACCFR                  = HDUL[0].header['FRAMEGRP']
        MACCDR                  = HDUL[0].header['DROPS']
    except:
        print('Error MACC parameters missing for file',path_files,file_name)
    
    RW_FrameID        = 0
    RW_RawlineID      = 0 
    RW_Rawline        = 0
    
    if INFO_WANTED_FROM_IMAGES['RAWLINES'] == True:
        try:
            RW_FrameID              = HDUL['RAWLINES'].data.FrameID.astype(int)
            RW_RawlineID            = HDUL['RAWLINES'].data.RawlineID.astype(int)
            RW_Rawline              = HDUL['RAWLINES'].data.Rawline.astype(int)
                    
        except:
            print('Error Rawlines missing for file',path_files,file_name)
        
            
    return MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET,RW_FrameID, RW_RawlineID, RW_Rawline,TSTART
        
def subtraction_badpx_chi2_sat_px(SCI_ALL,CHI2_ALL,BAD_PIX_MAPS,ref_pix,sci_pix,det_POSs,DET_ID,MACCGRP, MACCFR, MACCDR):
    SCI_sci                 = SCI_ALL[sci_pix] 
    CHI2_sci                = CHI2_ALL[sci_pix]
    
    for count,det_POS in enumerate (det_POSs):
        if DET_ID == det_POS:
        #  Setting the value of the signal to 0 ADU for the discarded pixels
            SCI_sci[(SCI_sci == 65535)]                      = 0
            SCI_sci[(BAD_PIX_MAPS[count,:,:].ravel() == 1)]  = 0
            
            if MACCGRP ==15 and MACCFR == 16 and MACCDR == 11:
                SCI_sci[(CHI2_sci > 47)]                     = 0  
                print('---------------SPEC MODE----------------MACC PARAM:',MACCGRP,MACCFR,MACCDR)
            else:
                print('---------------PHOT MODE----------------MACC PARAM:',MACCGRP,MACCFR,MACCDR)
                SCI_sci[(CHI2_sci == 1)]                     = 0  
            break

    return SCI_sci,CHI2_sci

def subtraction_dark(DARK_SCI,SCI_sci,sci_pix):
    SCI_sci = np.subtract(SCI_sci , DARK_SCI[sci_pix])
    
    return SCI_sci

###################################################################

"""
These functions provide some information relative to the search of the seed pixel of the PSF. 

The final output is:
- A text file Catalog with information relative to the different images analyzed and the different identified potential seed pixels.
- A scatter plot representing the distribution of the identified seed pixels in the X and Y axis.
"""

def search_for_PSF(SCI_sci, file_name, min_surrounding_ADU, amount_max_search, min_signal_seed_px, max_surrounding_fail, PID, DET_ID,count_seed_pixel_traffic_light,CAT_ALARMS):
    param_per_file = ([])
    start          = time.time()       
    print('---------------------- file name --------------------',file_name)   
    
    # Initiliazing the while loop traffic light and amount of iteration and surrounding pixel
    count_max_loop       = 0
    count_seed_pixel     = 0
    SCI_sci              = np.array(SCI_sci).reshape(2040,2040)
    
    while count_max_loop < amount_max_search :
        # Initializing the count of the pixels surrounding the seed pixels below min_surrounding_ADU
        count_surrounding    = 0
        
        # Identifying the max value of the image in an iterative process
        Max_flux_px          =  np.where(SCI_sci == np.max(SCI_sci))

        # incrementing the amount of maxima evaluated
        count_max_loop       += 1 
        
        if Max_flux_px[0][0] > 0 and Max_flux_px[0][0] < 2039 and Max_flux_px[1][0] > 0 and Max_flux_px[1][0] < 2039:
            maximum_and_surrounding_ADUs = SCI_sci[Max_flux_px[0][0]-1:Max_flux_px[0][0]+2,Max_flux_px[1][0]-1:Max_flux_px[1][0]+2].ravel()
            
            if maximum_and_surrounding_ADUs[4] > min_signal_seed_px:
                for maxima_and_surrounding_ADU in maximum_and_surrounding_ADUs:
                    if maxima_and_surrounding_ADU < min_surrounding_ADU:
                        count_surrounding += 1
                if count_surrounding > max_surrounding_fail + 1 :
                    SCI_sci[Max_flux_px[0][0],Max_flux_px[1][0]] = 0  # This set to zero the max flux that has not been associated to the PSF
                else:
                    print(count_surrounding,'OK This is kept','pix',Max_flux_px[0][0],Max_flux_px[1][0])
                    print(maximum_and_surrounding_ADUs)
                    
                    # counter maxima attributed to identified PSF
                    count_seed_pixel     += 1
                    param_per_file = np.array(str(str(file_name)[1:-1])[1:-1].replace("'",""))
                    param_per_file = np.append(param_per_file,PID)
                    param_per_file = np.append(param_per_file,DET_ID)
                    param_per_file = np.append(param_per_file,count_seed_pixel)
                    param_per_file = np.append(param_per_file,Max_flux_px[0][0])
                    param_per_file = np.append(param_per_file,Max_flux_px[1][0])
                    param_per_file = np.append(param_per_file,maximum_and_surrounding_ADUs)
                    
                    # This is to avoid multiple counting of the same cluster identifed as being a PSF
                    SCI_sci[Max_flux_px[0][0]-1:Max_flux_px[0][0]+2,Max_flux_px[1][0]-1:Max_flux_px[1][0]+2] = 0
                    
                    if count_seed_pixel_traffic_light == 0:  
                        CAT_ALARMS = np.array(param_per_file)[None,:]
                        count_seed_pixel_traffic_light = 1
                    else:
                        try:
                            CAT_ALARMS = np.concatenate((CAT_ALARMS,np.array(param_per_file)[None,:]))
                        except:
                            (print("No seed pixel identified",np.array(str(str(file_name)[1:-1])[1:-1].replace("'",""))))
            else:
                break
    end = time.time()
    print('Time of execution',end - start) 
    return CAT_ALARMS,count_seed_pixel_traffic_light


def save_cat_alarms(CAT_ALARMS,seconds,DET_ID):
    today = date.today()
    np.savetxt('Output/CAT_ALARMS_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds),CAT_ALARMS,delimiter=',',fmt="%s")
    return

def plot_seed_pixel_scatter(CAT_ALARMS,seconds,det_POSs):
    today = date.today()    
    count_per_det          = np.zeros(16)
    CAT_ALARMS_shape       = len(CAT_ALARMS)
    CAT_ALARMS_plot        = np.empty((16,CAT_ALARMS_shape,2))
    CAT_ALARMS_plot[:]     = np.NaN
    
    out_pdf = 'Output/Seed_pixel_scatter_{}_{}.pdf'.format(int(today.strftime("%d%m%Y")),seconds)
    pdf = matplotlib.backends.backend_pdf.PdfPages(out_pdf)
    
    fig = plt.figure(figsize=(15,15))
    fig.subplots_adjust(hspace=0, wspace=0)

    for i in range (CAT_ALARMS_shape):
        for count, det_POS in enumerate(det_POSs):
            if int(CAT_ALARMS[i,2]) == det_POS:
                CAT_ALARMS_plot[int(count),int(count_per_det[count]),0]  = CAT_ALARMS[i,4]
                CAT_ALARMS_plot[int(count),int(count_per_det[count]),1]  = CAT_ALARMS[i,5]
                count_per_det[count]+=1 
    for i in range (16):
        try:
            #xy = np.vstack([CAT_ALARMS_plot[i,:,0],CAT_ALARMS_plot[i,:,1]])
            #z = gaussian_kde(xy)(xy)
            ax = fig.add_subplot(4, 4, i+1)
            if i < 8 :
                ax.scatter(CAT_ALARMS_plot[i,:,0],CAT_ALARMS_plot[i,:,1])#,c=z, s=100)
                ax.set_xlim(0,2040)
                ax.set_ylim(0,2040)
            else:
                print(i,'LEVY')  
                ax.scatter(CAT_ALARMS_plot[i,:,0],CAT_ALARMS_plot[i,:,1])#,c=z, s=100)
                ax.set_xlim(0,2040)
                ax.set_ylim(2040,0)
            ax.set_title(('PSF seed pixels: the detector',det_POSs[i]))
            ax.set_xlabel('X position seed pixel',fontsize=18)
            ax.set_ylabel('Y position seed pixel',fontsize=18) 
        
        except: 
            print('PSF study ==> no data with detector',det_POSs[i])
            
    pdf.savefig()
    pdf.close()                                   

###################################################################

def get_time_axis_RL(MACCGRP,MACCFR,MACCDR):
    time_plot   = ([])
    frame_sec   = 1.48 
    time_plot_median = ([])
    
    for i in range (MACCGRP*MACCFR):
        for j in range (MACCGRP):
            if i < (j + 1) * MACCFR:
                time_plot = np.append(time_plot,j*MACCDR*frame_sec+i*frame_sec)
                break
                
    for j in range (MACCGRP):
        time_plot_median = np.append(time_plot_median,frame_sec*((j+1)*MACCFR+j*MACCDR)-MACCFR/2*1.48)

    return time_plot, time_plot_median

def get_sci_and_ref_pixels():
    #Definition of the frame of the reference pixels
    ref_pix = np.full((2048,2048), True)
    ref_pix [4:-4,4:-4] = False
    sci_pix = np.full((2048,2048), False)
    sci_pix [4:-4,4:-4] = True
    
    return ref_pix, sci_pix

def get_pos_detector():
    det_POSs  = [11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44]
    
    return det_POSs


def Select_file_from_PIDs_list(PIDs_list_file, PID_DARK, PIDs_ignored,path_files_IM,files_list_IM):
    #Getting the PIDs list, extracting the list from workdir and giving the amount of file into it
    PIDs_list                = np.genfromtxt('PIDs_list/' + PIDs_list_file)
    shape_list               = (len(files_list_from_PIDs))
    PIDs_list                = [79446,40644] #
    # Defining a unique variable (time in second) to identify the output
    seconds        = int(time.time())

    # loop over all the files of the directory
    i = 0
    while i < shape_list:
        if not fnmatch.fnmatch(files_list_from_PIDs[i], '*.lv1'):
            files_list = np.delete(files_list_from_PIDs,i,0)
        else:
            FITS_FILE = fits.open(path_files+files_list_from_PIDs[i]) 
            try:
                PID = FITS_FILE[0].header['PID1'] * 65536 + FITS_FILE[0].header['PID2'] 

                if PID == PID_DARK: 
                    print("file dark measurement (deleted from the list)",str(files_list_from_PIDs[i]))
                    files_list_from_PIDs = np.delete(files_list_from_PIDs,i,0)
                elif PID in PIDs_ignored:
                    files_list_from_PIDs = np.delete(files_list_from_PIDs,i,0)     
                #elif not int(FITS_FILE[0].header['SCE_POS']) == DET_SEL:
                #    files_list = np.delete(files_list,i,0)     
                else:
                    if not PID in (PIDs_list):
                        files_list_from_PIDs = np.delete(files_list_from_PIDs,i,0)
                    else:
                        i+=1   
            except:
                files_list_from_PIDs = np.delete(files_list_from_PIDs,i,0) 
        shape_list = (len(files_list_from_PIDs))

    today = date.today()
    np.savetxt('PIDs_list/files_selected_on_PIDs_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds) + PIDs_list_file,files_list_from_PIDs,fmt="%s")
    
    return files_list_from_PIDs, str(files_list_from_PIDs[i])

def Image_versus_HKTM(path_files_IM1, path_files_HK1, param_HK_list_user, INFO_WANTED_FROM_IMAGES, Rawlines_pix_lines, Rawlines_pix_rows, PIDs_list_file, PID_DARK, PIDs_ignored,T_MIN, T_MAX,Selection_files_mode, path_files_HK2, path_files_HK3, path_files_HK4, path_files_HK5, path_files_IM2, path_files_IM3, amount_paths_IM, amount_paths_HK):
    
    ############################### GETTING THE LIST OF IMAGES AND  HKTM FILES ############################
    #Loading the images
    print('Loading list files of images')
    files_list_IM1           = get_files_from_path(path_files_IM1)
    files_list_IM2           = get_files_from_path(path_files_IM2)
    files_list_IM3           = get_files_from_path(path_files_IM3)
    print('List files images loaded')
    
    print('Loading list files of HKTM')
    files_list_HK1           = get_files_from_path(path_files_HK1)
    files_list_HK2           = get_files_from_path(path_files_HK2)
    files_list_HK3           = get_files_from_path(path_files_HK3)
    files_list_HK4           = get_files_from_path(path_files_HK4)
    files_list_HK5           = get_files_from_path(path_files_HK5)
    print('List HKTM files loaded')
    
    # FOR TEST ONLY
    #files_list_IM           = files_list_IM[1000:2000]

    
    #Loading bad pixels maps 
    path_bad_pixels               = '/home/jovyan/shared/qe/'
    BAD_PIX_MAPS                  = get_bad_pixels_SCI(path_bad_pixels)

    #Shape of the list of the images
    if amount_paths_IM == 1:
        files_list_IM             = files_list_IM1
    if amount_paths_IM > 1 :
        files_list_IM             = np.append(files_list_IM1 , files_list_IM2)
        if amount_paths_IM > 2:
            files_list_IM         = np.append(files_list_IM , files_list_IM3)
    
    shape_list_IM                 = (len(files_list_IM))
    print('Number of files FITS images',shape_list_IM)

    # Getting positions of the science and reference pixels
    ref_pix,sci_pix               = get_sci_and_ref_pixels() 

    # Get an array with the position of the different detectors 
    det_POSs                      = get_pos_detector()
    
    T_MIN                         = time.mktime(datetime.strptime(T_MIN, '%Y-%m-%dT%H:%M:%S').timetuple()) 
    T_MAX                         = time.mktime(datetime.strptime(T_MAX, '%Y-%m-%dT%H:%M:%S').timetuple())
    
    # Initialisation of the final catalogs to be built with the information of interest
    CATALOG_INFO_IMAGES           = ([])
    CATALOG_INFO_IMAGES_CHANNELS  = ([])
    CATALOG_INFO_RAWLINES         = ([])
    CATALOG_HK_PARAM              = ([])
    
    try:
        PIDs_list = np.genfromtxt('PIDs_list/' + PIDs_list_file)
        print('PIDs list taken from external file')
    except:
        PIDs_list = PIDs_list_file
        print('PIDs list taken from array')
    
    # Defining a unique variable (time in second) to identify the output
    seconds  = time.strftime('%H%M%S', time.gmtime(int(time.time())))
    today    = date.today()
    
    # OTHER INITIAL SETTINGS
    param_HK_list_user_unit        = [None] * len(param_HK_list_user)
    keys                           = ['SCI_sci','SCI_ref','CHI2_sci','CHI2_ref']
    count_lines_catalog            = 0 
    count_file_bis                 = 0 
    count_seed_pixel_traffic_light = 0
    
    
    if INFO_WANTED_FROM_IMAGES['PSF_study'] == True:
        
        # Setting of variables for the research of the PSF
        min_surrounding_ADU  = 50 
        amount_max_search    = 100  
        min_signal_seed_px   = 200
        max_surrounding_fail = 3
        
        # Initialisation of the final catalog to be built with the information of interest
        CAT_ALARMS = ([])
        
        print('Parameters for PSF search //','min_surrounding_ADU:',min_surrounding_ADU,'amount_max_search:',
              amount_max_search,'min_signal_seed_px:', min_signal_seed_px,
              'max_surrounding_fail:', max_surrounding_fail)

    # Initializing the loop over all the files in the folder with "path_files_IM"
    for count_file, file_name in enumerate(files_list_IM):
        start = time.time() 
        
        try: 
            HDUL,SCI_ALL,CHI2_ALL,PID,DET_ID  = get_param_from_fits_file(path_files_IM1, path_files_IM2, path_files_IM3, file_name)
        except:
            if Selection_files_mode['Select_files_from_PIDs_list'] == True:
                PID = 0
                print('TEST PID', PID)
                try:
                    PID                     = HDUL[0].header['PID1'] * 65536 + HDUL[0].header['PID2']
                    print('PIDs_list from the selection mode',PIDs_list, PID)
                    if PID in (PIDs_list): 
                        print('PID from the selection mode',path_files,file_name,'// PID',PID)  
                        SCI_ALL          = HDUL['SCIENCE'].data.astype(int)
                        DET_ID           = int(HDUL[0].header['SCE_POS']) 
                        CHI2_ALL         = np.zeros((2048,2048))
                except:
                    print('Error PID missing for file (PID selection mode)',path_files,file_name,'// PID',PID)  
                    continue
            else:
                print('Missing info in FITS file of the image', file_name)
                continue
                  
        try:
            MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET, RW_FrameID, RW_RawlineID, RW_Rawline,TSTART  = get_param_from_fits_file_BIS(HDUL, INFO_WANTED_FROM_IMAGES)
        except:
            if Selection_files_mode['Select_files_from_PIDs_list'] == True:    
                if PID in (PIDs_list): 
                    try:
                        TSTART                  = HDUL[0].header['TSTART']
                        DATE_OBS_sec_CET        = time.mktime(datetime.strptime(HDUL[0].header['DATE-OBS'], '%Y-%m-%dT%H:%M:%S').timetuple())
                        try:
                            MACCGRP             = HDUL[0].header['GRP_CNT']
                            MACCFR              = HDUL[0].header['FRAMEGRP']
                            MACCDR              = HDUL[0].header['DROPS']
                        except:
                            print('Error MACC parameters missing for file (PID slection mode)',path_files,file_name)
                    except:
                        print('Error DATE OBS parameters missing for file (PID slection mode)',path_files,file_name)
                        continue
            else:
                print('Missing info in FITS file of the image BIS', file_name)
                continue
            
        if Selection_files_mode['Select_files_from_PIDs_list'] == True:
            #print('PIDs_list',PIDs_list)
            if PID in PIDs_ignored:
                continue
            if not PID in (PIDs_list): 
                #print('PID',PID)
                continue 
            else:
                print('PIDPIDPIDPIDPID',PID)
                
        if PID in (PIDs_list): 
            print('PIDPIDPIDPIDPID',PID)
            
        if Selection_files_mode['Select_files_on_date'] == True:
            if not T_MIN < DATE_OBS_sec_CET < T_MAX :
                print(file_name, 'is out of the time interval',T_MIN,DATE_OBS_sec_CET,T_MAX)
                continue
            else:
                print(file_name, 'is inside the time interval',T_MIN,DATE_OBS_sec_CET,T_MAX)
                
        if Selection_files_mode['Detector_selection'] == True:
            if not DET_ID in Detectors_to_process:
                continue
        
        SCI_sci,CHI2_sci = subtraction_badpx_chi2_sat_px(SCI_ALL, CHI2_ALL, BAD_PIX_MAPS, ref_pix, sci_pix,det_POSs,DET_ID,MACCGRP, MACCFR, MACCDR) 

        if any([INFO_WANTED_FROM_IMAGES.get(key) for key in keys]) == True:
            CATALOG_INFO_IMAGES           = create_catalog_IMAGES(SCI_ALL, CHI2_ALL, SCI_sci,CHI2_sci,PID,DET_ID,ref_pix,MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET,TSTART,CATALOG_INFO_IMAGES,count_file_bis)
            
        if INFO_WANTED_FROM_IMAGES['CHANNELS'] == True:
            CATALOG_INFO_IMAGES_CHANNELS      = create_catalog_IMAGES_CHANNELS(SCI_ALL,CHI2_ALL, DET_ID, MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET,CATALOG_INFO_IMAGES_CHANNELS,PID,det_POSs,count_file_bis)
            
        if INFO_WANTED_FROM_IMAGES['RAWLINES'] == True:
            CATALOG_INFO_RAWLINES, count_lines_catalog  = Create_catalog_RAWLINES(RW_FrameID, RW_RawlineID, RW_Rawline, DET_ID, DATE_OBS_sec_CET,CATALOG_INFO_RAWLINES,MACCGRP,MACCFR,MACCDR, det_POSs, Rawlines_pix_lines, Rawlines_pix_rows,count_lines_catalog) 
        
        if INFO_WANTED_FROM_IMAGES['PSF_study'] == True:
            CAT_ALARMS,count_seed_pixel_traffic_light = search_for_PSF(SCI_sci, file_name, min_surrounding_ADU, amount_max_search, min_signal_seed_px, max_surrounding_fail, PID, DET_ID,count_seed_pixel_traffic_light,CAT_ALARMS)
                
        count_file_bis += 1
        end = time.time()
        print('Time of execution',end - start) 
               
    if Selection_files_mode['Select_files_on_date'] == False:
        try:
            t_min = np.min (CATALOG_INFO_IMAGES[:,6])
            t_max = np.max (CATALOG_INFO_IMAGES[:,6])
        except:
            try:
                t_min = np.min (CATALOG_INFO_IMAGES_CHANNELS[:,1])
                t_max = np.max (CATALOG_INFO_IMAGES_CHANNELS[:,1])
            except:
                print('No data from the FITS files of the images')
                t_min = int(input("Please enter a the t_min in seconds passed since epoch in CET to select HK parameters:\n"))
                t_max = int(input("Please enter a the t_max in seconds passed since epoch in CET to select HK parameters:\n"))
    else:
        t_min = T_MIN
        t_max = T_MAX
        
    print('param_HK_list_user', param_HK_list_user[0])  
    
    CATALOG_HK_PARAM, param_HK_list_user_unit  = get_HK_param(path_files_HK1, files_list_HK1, t_min, t_max, param_HK_list_user,param_HK_list_user_unit, path_files_HK2, files_list_HK2, path_files_HK3, files_list_HK3, path_files_HK4, files_list_HK4, path_files_HK5, files_list_HK5,amount_paths_HK)

    today = date.today()
    
    if any([INFO_WANTED_FROM_IMAGES.get(key) for key in keys]) == True:
        np.savetxt('Output/CATALOG_INFO_IMAGES_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds), CATALOG_INFO_IMAGES)
        
    if INFO_WANTED_FROM_IMAGES['CHANNELS'] == True:    
        np.savetxt('Output/CATALOG_INFO_IMAGES_CHANNELS_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds), CATALOG_INFO_IMAGES_CHANNELS)
        
    if INFO_WANTED_FROM_IMAGES['RAWLINES'] == True:  
        np.savetxt('Output/CATALOG_INFO_RAWLINES_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds), CATALOG_INFO_RAWLINES)
    
    try:
        print(param_HK_list_user[0])    
        np.savetxt('Output/CATALOG_HK_PARAM_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds), CATALOG_HK_PARAM)
    except:
        True
        
    if INFO_WANTED_FROM_IMAGES['PSF_study'] == True:    
        save_cat_alarms(CAT_ALARMS,seconds,DET_ID) 
        plot_seed_pixel_scatter(CAT_ALARMS,seconds,det_POSs) 
    
    plots_Images_VS_HK(CATALOG_INFO_IMAGES, CATALOG_INFO_IMAGES_CHANNELS, CATALOG_INFO_RAWLINES, CATALOG_HK_PARAM, det_POSs,seconds,param_HK_list_user, t_min, t_max, param_HK_list_user_unit,keys,INFO_WANTED_FROM_IMAGES, Rawlines_pix_lines, Rawlines_pix_rows)
        
    return #CATALOG_INFO_IMAGES, CATALOG_INFO_IMAGES_CHANNELS, CATALOG_INFO_RAWLINES, CATALOG_HK_PARAM, t_min, t_max,BAD_PIX_MAPS,seconds

def create_catalog_IMAGES(SCI_ALL, CHI2_ALL, SCI_sci,CHI2_sci,PID,DET_ID,ref_pix,MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET,TSTART,CATALOG_INFO_IMAGES,count_file_bis):  
    
    param_per_file = ([])
    REF_PIX_FRAME_SCI  = SCI_ALL[ref_pix]  
    REF_PIX_FRAME_CHI2 = CHI2_ALL[ref_pix]
    
    param_per_file.append(PID)
    param_per_file.append(TSTART)
    param_per_file.append(np.nanmedian(SCI_sci))
    param_per_file.append(np.nanvar(SCI_sci,ddof=1))
    param_per_file.append(DET_ID)
    param_per_file.append((MACCGRP-1)*(MACCFR+MACCDR))
    param_per_file.append(DATE_OBS_sec_CET)
    param_per_file.append(np.nanmedian(REF_PIX_FRAME_SCI))
    param_per_file.append(np.nanvar(REF_PIX_FRAME_SCI,ddof=1))                                  
    param_per_file.append(np.nanmedian(CHI2_sci))
    param_per_file.append(np.nanvar(CHI2_sci,ddof=1))
    param_per_file.append(np.nanmedian(REF_PIX_FRAME_CHI2))                         
    param_per_file.append(np.nanvar(REF_PIX_FRAME_CHI2,ddof=1))  
    
    #param_per_file.append(np.median(ALL_FRAME_SCI[:4,:]))
    #param_per_file.append(np.median(ALL_FRAME_SCI[-4:,:]))
    #param_per_file.append(np.median(ALL_FRAME_SCI[4:-4,-4:]))
    #param_per_file.append(np.median(ALL_FRAME_SCI[4:-4,:4]))
    
    if count_file_bis == 0 :  
        CATALOG_INFO_IMAGES = np.array(param_per_file)[None,:]
    else:
        CATALOG_INFO_IMAGES = np.concatenate((CATALOG_INFO_IMAGES,np.array(param_per_file)[None,:]))
    
    return CATALOG_INFO_IMAGES



def get_HK_param(path_files_HK1, files_list_HK1, t_min, t_max, param_HK_list_user,param_HK_list_user_unit, path_files_HK2, files_list_HK2, path_files_HK3, files_list_HK3, path_files_HK4, files_list_HK4, path_files_HK5, files_list_HK5, amount_paths_HK):
    
    if amount_paths_HK == 1:
        files_list_HK   = files_list_HK1
    if amount_paths_HK > 1:
        files_list_HK           = np.append(files_list_HK1 , files_list_HK2)
        if amount_paths_HK > 2:
            files_list_HK           = np.append(files_list_HK , files_list_HK3)
            if amount_paths_HK > 3:
                files_list_HK           = np.append(files_list_HK , files_list_HK4)   
                if amount_paths_HK > 4:
                    files_list_HK           = np.append(files_list_HK , files_list_HK5)
    
    shape_list_HK           = (len(files_list_HK))
    print('files_list_HK',files_list_HK)
    param_list              = get_all_HK_param()
    print('number of files HKTM',shape_list_HK)
    
    shape_list_HK           = (len(files_list_HK))

    CATALOG_HK_PARAM        = np.empty((50000,len(param_HK_list_user) + 2))
    CATALOG_HK_PARAM[:]     = np.NaN
    
    traffic_count           = 0
    
    i = 0 
    while i < shape_list_HK:
        
        if not fnmatch.fnmatch(files_list_HK[i], '*001_000*') and not fnmatch.fnmatch(files_list_HK[i], '*002_000*') and not fnmatch.fnmatch(files_list_HK[i], '*003_000*') and not fnmatch.fnmatch(files_list_HK[i], '*004_000*'):
            print(files_list_HK[i])
            files_list_HK   = np.delete(files_list_HK,i,0)
            
        else:
            #print(path_files_HK + files_list_HK[i])  
            #if os.path.isfile(path_files_HK + files_list_HK[i]):
            try: 
                HDUL_HK     = fits.open(path_files_HK1 + files_list_HK[i])
            except:
                print('No HKTM file found in',path_files_HK1 + files_list_HK[i])
                try:
                    HDUL_HK     = fits.open(path_files_HK2 + files_list_HK[i])
                except:
                    print('No HKTM file found in',path_files_HK2 + files_list_HK[i])
                    try: 
                        HDUL_HK     = fits.open(path_files_HK3 + files_list_HK[i])
                    except:
                        print('No HKTM file found in',path_files_HK3 + files_list_HK[i])
                        try: 
                            HDUL_HK     = fits.open(path_files_HK4 + files_list_HK[i])
                        except:
                            print('No HKTM file found in',path_files_HK4 + files_list_HK[i])
                            try: 
                                HDUL_HK     = fits.open(path_files_HK5 + files_list_HK[i])
                            except:
                                print('Error while searching the path / file :',path_files_HK5 + files_list_HK[i])
                                
            Header_HK   = HDUL_HK[1].header[:]
            traffic_check = 0
            
            for count_param, param in enumerate(param_HK_list_user):
                traffic_light = 1
                try:
                    PARAM_COL = HDUL_HK[1].data['CALST{}'.format(param)]
                except:
                    traffic_light = 0
                    
                if traffic_light == 1:
                    if  param_HK_list_user_unit[count_param] == None:
                        for k in range (len(Header_HK)):
                            if Header_HK[k] == 'CALST{}'.format(param):
                                #param_HK_list_user_unit[count_param] = unit
                                try:
                                    unit = Header_HK['TUNIT{}'.format(re.findall(r'\d+',list(Header_HK.keys())[k])[0])]
                                    param_HK_list_user_unit[count_param] = unit
                                    print('Unit found for ',param_list[param],unit)
                                except:
                                    print('No unit found for ',param_list[param])
                                    param_HK_list_user_unit[count_param] = '.'
                                break
                                    
            if traffic_light == 1: 
                
                Time_COARS  = HDUL_HK[1].data.OBTCOARS
                
                for j in range (len(HDUL_HK[1].data)):
                    if t_min < Time_COARS[j] < t_max:
                        CATALOG_HK_PARAM[traffic_count,0] = Time_COARS[j]
                        CATALOG_HK_PARAM[traffic_count,1] = HDUL_HK[1].data['CALST0001'] [j]
                        for count_param, param in enumerate(param_HK_list_user):
                            #check_place = CATALOG_HK_PARAM[:,0].tolist().index(Time_COARS[j])
                            PARAM_COL = HDUL_HK[1].data['CALST{}'.format(param)]
                            CATALOG_HK_PARAM[traffic_count,count_param + 2] = PARAM_COL[j]
                            
                        traffic_count += 1 
                        
            i += 1
            
        shape_list_HK  = (len(files_list_HK))
    
    return CATALOG_HK_PARAM, param_HK_list_user_unit
                                        
def create_catalog_IMAGES_CHANNELS(SCI_ALL,CHI2_ALL, DET_ID, MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET,CATALOG_INFO_IMAGES_CHANNELS,PID,det_POSs,count_file_bis):   
    param_per_file_channels = []
    param_per_file_channels.append(PID)
    param_per_file_channels.append(DATE_OBS_sec_CET)
    param_per_file_channels.append(DET_ID)
    param_per_file_channels.append((MACCGRP-1)*(MACCFR+MACCDR))
    
    for count_channel in range (32):
        if count_channel == 0 :
            param_per_file_channels.append(np.nanmedian((SCI_ALL[4:(count_channel+1)*64,4:-4])))
        elif count_channel == 31 :
            param_per_file_channels.append(np.nanmedian((SCI_ALL[count_channel*64:-4,4:-4])))
        else:
            param_per_file_channels.append(np.nanmedian((SCI_ALL[count_channel*64:(count_channel+1)*64,4:-4])))
            
    for count_channel in range (32):  
        if count_channel == 0 :
            param_per_file_channels.append(np.nanmedian((CHI2_ALL[4:(count_channel+1)*64,4:-4])))
        elif count_channel == 31 :
            param_per_file_channels.append(np.nanmedian((CHI2_ALL[count_channel*64:-4,4:-4])))
        else:
            param_per_file_channels.append(np.nanmedian((CHI2_ALL[count_channel*64:(count_channel+1)*64,4:-4])))
    
    if count_file_bis == 0 :  
        CATALOG_INFO_IMAGES_CHANNELS = np.array(param_per_file_channels)[None,:]
    else:
        CATALOG_INFO_IMAGES_CHANNELS = np.concatenate((CATALOG_INFO_IMAGES_CHANNELS,np.array(param_per_file_channels)[None,:]))             
    
    return CATALOG_INFO_IMAGES_CHANNELS

def Create_catalog_RAWLINES(RW_FrameID, RW_RawlineID, RW_Rawline, DET_ID, DATE_OBS_sec_CET,CATALOG_INFO_RAWLINES,MACCGRP,MACCFR,MACCDR,det_POSs,Rawlines_pix_lines, Rawlines_pix_rows,count_lines_catalog):
    
    time_plot,time_plot_median   = get_time_axis_RL(MACCGRP,MACCFR,MACCDR)
    count_time_plot              = 0
    param_per_frame_rawlines     = []
    
    j = 0
    
    while j < RW_FrameID.shape[0]:
        traffic_light_rawline = 0
        for count_rawlines_lines, Rawlines_pix_line in enumerate(Rawlines_pix_lines):
            if RW_RawlineID[j] == Rawlines_pix_line:
                traffic_light_rawline = 1
                if count_rawlines_lines == 0 :
                    param_per_frame_rawlines.append(DATE_OBS_sec_CET + time_plot[count_time_plot])
                    param_per_frame_rawlines.append(DET_ID)
                    count_time_plot += 1
                for count_rawlines_rows, Rawlines_pix_row  in  enumerate(Rawlines_pix_rows):
                    param_per_frame_rawlines.append(RW_Rawline[j][Rawlines_pix_row])
                break
                
        if traffic_light_rawline == 1 :       
            if count_rawlines_lines == len(Rawlines_pix_lines) - 1:
                if count_lines_catalog == 0 :
                    CATALOG_INFO_RAWLINES    = np.array(param_per_frame_rawlines)[None,:]
                    param_per_frame_rawlines = []
                    count_lines_catalog      = 1
                else:
                    CATALOG_INFO_RAWLINES    = np.concatenate((CATALOG_INFO_RAWLINES,np.array(param_per_frame_rawlines)[None,:])) 
                    param_per_frame_rawlines = []
        j += 1
        
    return CATALOG_INFO_RAWLINES,count_lines_catalog
        
def get_xlabel_TOBS(t_min,t_max):
    TOBS_secs          = np.arange(t_min ,t_max,(t_max-t_min)/10)
    TOBS_for_plots     = ([])
    
    for i in range (len(TOBS_secs)):
        TOBS_for_plots = np.append(TOBS_for_plots,time.strftime('%D:%H:%M:%S', time.gmtime(TOBS_secs[i])))
        
    return TOBS_secs,TOBS_for_plots

def plots_Images_VS_HK(CATALOG_INFO_IMAGES, CATALOG_INFO_IMAGES_CHANNELS, CATALOG_INFO_RAWLINES, CATALOG_HK_PARAM, det_POSs,seconds,param_HK_list_user, t_min, t_max, param_HK_list_user_unit,keys,INFO_WANTED_FROM_IMAGES, Rawlines_pix_lines, Rawlines_pix_rows):
    
    TOBS_secs,TOBS_for_plots = get_xlabel_TOBS(t_min,t_max)
    
    x_formatter = FixedFormatter(TOBS_for_plots)
    x_locator   = FixedLocator(TOBS_secs)
    
    if any([INFO_WANTED_FROM_IMAGES.get(key) for key in keys]) == True:
        CATALOG_INFO_IMAGES_plot          = CATALOG_INFO_IMAGES_PLOT_prep(CATALOG_INFO_IMAGES,det_POSs)
        
    if INFO_WANTED_FROM_IMAGES['CHANNELS'] == True:
        CATALOG_INFO_IMAGES_CHANNELS_plot = CATALOG_INFO_IMAGES_CHANNELS_prep(CATALOG_INFO_IMAGES_CHANNELS,det_POSs)
    
    if INFO_WANTED_FROM_IMAGES['RAWLINES'] == True:   
        CATALOG_INFO_RAWLINES_plot = CATALOG_INFO_RAWLINES_prep(CATALOG_INFO_RAWLINES,det_POSs)
    try:
        CATALOG_HK_PARAM = CATALOG_HK_PARAM[:,:][np.argsort(CATALOG_HK_PARAM[:, 0])]
    except:
        True
    today      = date.today() 
    param_list = get_all_HK_param()
    
    out_pdf    = 'Output/Images_VS_HK_{}_{}.pdf'.format(int(today.strftime("%d%m%Y")),seconds)
    pdf        = matplotlib.backends.backend_pdf.PdfPages(out_pdf)
    
    
    if INFO_WANTED_FROM_IMAGES['SCI_sci'] == True:
        SCI_PLOT = 1
    else:
        SCI_PLOT = 0
    
    if INFO_WANTED_FROM_IMAGES['CHI2_ref'] == True:
        CHI_PLOT = 1
    else:
        CHI_PLOT = 0
    
    if INFO_WANTED_FROM_IMAGES['SCI_ref'] == True:
        SCI_ref_PLOT = 1
    else:
        SCI_ref_PLOT = 0
    
    if INFO_WANTED_FROM_IMAGES['CHI2_sci'] == True:
        CHI_ref_PLOT = 1
    else:
        CHI_ref_PLOT = 0
    
    if INFO_WANTED_FROM_IMAGES['DPU1'] == True:
        DPU1_PLOT = 1
    else:
        DPU1_PLOT = 0
    
    if INFO_WANTED_FROM_IMAGES['DPU2'] == True:
        DPU2_PLOT = 1
    else:
        DPU2_PLOT = 0
    
    if INFO_WANTED_FROM_IMAGES['CHANNELS'] == True:
        CHANNELS_PLOT = 1
    else:
        CHANNELS_PLOT = 0
        
    if INFO_WANTED_FROM_IMAGES['RAWLINES'] == True:
        RAWLINES_PLOT = 1 
    else:
        RAWLINES_PLOT = 0

    try:
        HK_PLOTS = len(param_HK_list_user)
        print('plotting {} HK parameters'.format(HK_PLOTS))
    except:
        print('No HK parameters to be plotted')
        HK_PLOTS = 0
        
    count_plots = HK_PLOTS + (SCI_PLOT + CHI_PLOT) * (DPU1_PLOT + DPU2_PLOT) + 8 * (CHANNELS_PLOT) * ((DPU1_PLOT + DPU2_PLOT)) + (SCI_ref_PLOT + CHI_ref_PLOT) * (DPU1_PLOT + DPU2_PLOT) + RAWLINES_PLOT * (len(Rawlines_pix_rows) * len(Rawlines_pix_lines)) * (DPU1_PLOT + DPU2_PLOT)
    
    if count_plots <= 15 :
        fig     = plt.figure(figsize=(15,40))
        fig.subplots_adjust(hspace=0, wspace=0)
        ax = fig.subplots (count_plots, 1)
        print('-----SHORT PLOT----')
    elif count_plots > 30 :
        fig     = plt.figure(figsize=(15,120))
        fig.subplots_adjust(hspace=0, wspace=0)
        ax = fig.subplots (count_plots, 1)
        print('-----VERY LONG PLOT----',count_plots)
    else: 
        fig     = plt.figure(figsize=(15,80))
        fig.subplots_adjust(hspace=0, wspace=0)
        ax = fig.subplots (count_plots, 1)
        print('-----LONG PLOT----',count_plots)
    count_traffic_plots = -1
    
    if INFO_WANTED_FROM_IMAGES['SCI_sci'] == True :
        if INFO_WANTED_FROM_IMAGES['DPU1'] == True :
            count_traffic_plots += 1
            for i in range (8):
                try: 
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,2] - 1024 ) / CATALOG_INFO_IMAGES_plot [i,:,5],'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_ylabel('DPU1 Signal science in ADU/fr/px')
                    ax[count_traffic_plots].set_xticklabels([])
                except:
                    print('No data to plot SCI_sci for the Detector {}'.format(det_POSs[i]))
        if INFO_WANTED_FROM_IMAGES['DPU2'] == True :
            count_traffic_plots += 1
            for i in range (8,16):
                try:
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,2] - 1024 ) / CATALOG_INFO_IMAGES_plot [i,:,5],'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max) 
                    ax[count_traffic_plots].set_ylabel('DPU2 Signal science in ADU/fr/px')
                    ax[count_traffic_plots].set_xticklabels([])
                except:
                    print('No data to plot SCI_sci for the Detector {}'.format(det_POSs[i]))
                    
    if INFO_WANTED_FROM_IMAGES['CHI2_sci'] == True :
        if INFO_WANTED_FROM_IMAGES['DPU1'] == True :
            count_traffic_plots += 1
            for i in range (8):
                try:
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,9]),'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_ylabel('DPU1 CHI2 science')
                    ax[count_traffic_plots].set_xticklabels([])
                                       
                except:
                    print('No data to plot CHI2_sci for the Detector {}'.format(det_POSs[i]))
        if INFO_WANTED_FROM_IMAGES['DPU2'] == True :
            count_traffic_plots += 1          
            for i in range (8,16):
                try:
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,9]),'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max) 
                    ax[count_traffic_plots].set_ylabel('DPU2 CHI2 science')
                    ax[count_traffic_plots].set_xticklabels([])
                except:
                    print('No data to plot CHI2_sci for the Detector {}'.format(det_POSs[i]))
                    
    if INFO_WANTED_FROM_IMAGES['SCI_ref'] == True :
        if INFO_WANTED_FROM_IMAGES['DPU1'] == True :
            count_traffic_plots += 1  
            for i in range (8):
                try:
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,7] - 1024 ) / CATALOG_INFO_IMAGES_plot [i,:,5],'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_ylabel('DPU1 Signal ref in ADU/fr/px')
                    ax[count_traffic_plots].set_xticklabels([])                    
                except:
                    print('No data to plot SCI_ref for the Detector {}'.format(det_POSs[i]))
        if INFO_WANTED_FROM_IMAGES['DPU2'] == True :
            count_traffic_plots += 1  
            for i in range (8,16):
                try:
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,7] - 1024 ) / CATALOG_INFO_IMAGES_plot [i,:,5],'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max) 
                    ax[count_traffic_plots].set_ylabel('DPU2 Signal ref in ADU/fr/px')
                    ax[count_traffic_plots].set_xticklabels([])
                except:
                    print('No data to plot SCI_ref for the Detector {}'.format(det_POSs[i]))
                    
    if INFO_WANTED_FROM_IMAGES['CHI2_ref'] == True :
        if INFO_WANTED_FROM_IMAGES['DPU1'] == True :
            count_traffic_plots += 1  
            for i in range (8):
                try:
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,11]),'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_ylabel('DPU1 CHI2 ref')        
                    ax[count_traffic_plots].set_xticklabels([])
                except:
                    print('No data to plot CHI_ref for the Detector {}'.format(det_POSs[i]))
        if INFO_WANTED_FROM_IMAGES['DPU2'] == True :
            count_traffic_plots += 1  
            for i in range (8,16):
                try:
                    ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_plot[i,:,6], (CATALOG_INFO_IMAGES_plot[i,:,11]),'o-', label = 'Detector {}'.format(det_POSs[i]))
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_ylabel('DPU2 CHI2 ref')  
                    ax[count_traffic_plots].set_xticklabels([])
                except:
                        print('No data to plot CHI_ref for the Detector {}'.format(det_POSs[i]))
                        
    if INFO_WANTED_FROM_IMAGES['CHANNELS'] == True :
        if INFO_WANTED_FROM_IMAGES['DPU1'] == True :
            for i in range (8):
                try:
                    count_traffic_plots += 1 
                    ax[count_traffic_plots].text(.5,.8,'Study per channel → Detector {}'.format(det_POSs[i]),fontsize=10, horizontalalignment='center',  transform=ax[count_traffic_plots].transAxes)
                    ax[count_traffic_plots].set_ylabel('DPU1 Signal in ADU/fr/px') 
                    for ii in range (32):
                        ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,1], (CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,ii + 4] - 1024 ) / CATALOG_INFO_IMAGES_CHANNELS_plot [i,:,3],'o-', label = 'Channel {}'.format(ii))
                        ax[count_traffic_plots].legend(ncol=3,fontsize = 6)
                        ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_xticklabels([])  
                except:
                    print('No data to plot CHANNELS for the Detector {}'.format(det_POSs[i]))
                    
        if INFO_WANTED_FROM_IMAGES['DPU2'] == True :
            for i in range (8,16):
                try:
                    count_traffic_plots += 1  
                    ax[count_traffic_plots].text(.5,.8,'Study per channel → Detector {}'.format(det_POSs[i]),fontsize=10, horizontalalignment='center',  transform=ax[count_traffic_plots].transAxes)
                    ax[count_traffic_plots].set_ylabel('DPU2 Signal in ADU/fr/px') 
                    for ii in range (32):
                        ax[count_traffic_plots].plot(CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,1], (CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,ii + 4] - 1024 ) / CATALOG_INFO_IMAGES_CHANNELS_plot [i,:,3],'o-', label = 'Channels {}'.format(ii))
                        ax[count_traffic_plots].legend(ncol=3,fontsize = 6)
                        ax[count_traffic_plots].set_xlim(t_min, t_max) 
                    ax[count_traffic_plots].set_xticklabels([])
                except:
                    print('No data to plot CHANNELS for the Detector {}'.format(det_POSs[i]))
                    
    if INFO_WANTED_FROM_IMAGES['RAWLINES'] == True :
        if INFO_WANTED_FROM_IMAGES['DPU1'] == True :
            for count_lines_RAWLINES, Rawlines_pix_line in enumerate(Rawlines_pix_lines) :
                for count_ROWS_RAWLINES, Rawlines_pix_row in enumerate(Rawlines_pix_rows) :
                    count_traffic_plots += 1 
                    ax[count_traffic_plots].set_ylabel('RAWLINES in ADU/px') 
                    ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_xticklabels([])
                    ax[count_traffic_plots].text(.5,.8,'RAWLINES PIXEL ({};{})'.format(Rawlines_pix_line,Rawlines_pix_row),fontsize=10, horizontalalignment='center',  transform=ax[count_traffic_plots].transAxes)
                    for i in range (8):
                        try:
                            ax[count_traffic_plots].plot(CATALOG_INFO_RAWLINES_plot[i,:,0], CATALOG_INFO_RAWLINES_plot[i,:,2 + count_lines_RAWLINES*len(Rawlines_pix_rows) + count_ROWS_RAWLINES],'o-', label = 'Detector {}'.format(det_POSs[i])) 
                        except:
                            print('No data to plot RAWLINES for the Detector {}'.format(det_POSs[i]))   
                    ax[count_traffic_plots].legend(ncol=3)

        if INFO_WANTED_FROM_IMAGES['DPU2'] == True :
            for count_lines_RAWLINES, Rawlines_pix_line in enumerate(Rawlines_pix_lines) :
                for count_ROWS_RAWLINES, Rawlines_pix_row in enumerate(Rawlines_pix_rows) :
                    count_traffic_plots += 1 
                    ax[count_traffic_plots].set_ylabel('RAWLINES in ADU/px') 
                    ax[count_traffic_plots].legend(ncol=3)
                    ax[count_traffic_plots].set_xlim(t_min, t_max)
                    ax[count_traffic_plots].set_xticklabels([])
                    ax[count_traffic_plots].text(.5,.8,'RAWLINES PIXEL ({};{})'.format(Rawlines_pix_line,Rawlines_pix_row),fontsize=10, horizontalalignment='center',  transform=ax[count_traffic_plots].transAxes)
                    for i in range (8,16):
                        try:
                            ax[count_traffic_plots].plot(CATALOG_INFO_RAWLINES_plot[i,:,0], CATALOG_INFO_RAWLINES_plot[i,:,2 + count_lines_RAWLINES*len(Rawlines_pix_rows) + count_ROWS_RAWLINES],'o-', label = 'Detector {}'.format(det_POSs[i]))
                        except:
                            print('No data to plot RAWLINES for the Detector {}'.format(det_POSs[i])) 
                    ax[count_traffic_plots].legend(ncol=3)
        
    try:
        param_HK_list_user[0]
        print(CATALOG_HK_PARAM[:,0],'TEST')
        count_traffic_plots += 1 
    except:
        print('No data form the HK parameters to be plotted')
        
    count_param = 0   
    for count_param, param in enumerate(param_HK_list_user):
        try:
            ax[count_param+count_traffic_plots].plot(CATALOG_HK_PARAM[:,0],CATALOG_HK_PARAM[:,count_param + 2], label = 'NIST{}:'.format(param) + param_list['{}'.format(param)])
        except:
            ax[count_param+count_traffic_plots].plot(CATALOG_HK_PARAM[:,0],CATALOG_HK_PARAM[:,count_param + 2])
        ax[count_param+count_traffic_plots].legend(ncol=3)
        ax[count_traffic_plots].set_xticklabels([])
        ax[count_param+count_traffic_plots].set_ylabel(param_HK_list_user_unit[count_param])
        ax[count_param+count_traffic_plots].set_xlim(t_min, t_max)
        print(count_plots,count_param+count_traffic_plots)
            
    ax[count_param+count_traffic_plots].xaxis.set_major_formatter(x_formatter)
    ax[count_param+count_traffic_plots].xaxis.set_major_locator(x_locator)
    
    ax[0].xaxis.set_major_formatter(x_formatter)
    ax[0].xaxis.set_major_locator(x_locator)
    
    ax[count_param+count_traffic_plots].tick_params(axis='both',which='both', left=True, top=False, right=False, bottom=True,
                            length=10,width=2,pad=10,direction='inout',
                    labelbottom=True,labeltop=False,labelleft=True)
    ax[0].tick_params(axis='both',which='both', left=True, top=True, right=False, bottom=False,
                            length=10,width=2,pad=10,direction='inout',
                    labelbottom=False,labeltop=True,labelleft=True)
    for tick in ax[0].get_xticklabels():
        tick.set_rotation(45)
        
    for tick in ax[count_param+count_traffic_plots].get_xticklabels():
        tick.set_rotation(45)  
    
    if INFO_WANTED_FROM_IMAGES['Check_time'] == True:   
        check_time_images_HKTM(CATALOG_INFO_IMAGES, CATALOG_HK_PARAM, x_formatter, x_locator,today, seconds, t_min, t_max)
        
    pdf.savefig(fig)
    pdf.close()

def get_files_from_path(path_files):
    files_list     =  os.listdir(path_files)    
    print(path_files, 'contains', len(files_list),'files')
    
    return files_list
    
def CATALOG_INFO_IMAGES_PLOT_prep(CATALOG_INFO_IMAGES,det_POSs):
    count_per_det                = np.zeros(16)
    try:
        CATALOG_INFO_IMAGES_shape    = CATALOG_INFO_IMAGES.shape[0]
    except:
        print('Error: no info has been taken from the images')
        
    CATALOG_INFO_IMAGES_shapeCOL = CATALOG_INFO_IMAGES.shape[1]
    CATALOG_INFO_IMAGES_plot     = np.empty((16,CATALOG_INFO_IMAGES_shape,CATALOG_INFO_IMAGES_shapeCOL))
    CATALOG_INFO_IMAGES_plot[:]  = np.NaN
    
    for i in range (CATALOG_INFO_IMAGES_shape):
        for count, det_POS in enumerate(det_POSs):
            if int(CATALOG_INFO_IMAGES[i,4]) == det_POS:
                for j in range (CATALOG_INFO_IMAGES_shapeCOL):
                    CATALOG_INFO_IMAGES_plot[int(count),int(count_per_det[count]),j]  = CATALOG_INFO_IMAGES[i,j]
                count_per_det[count]+=1   
                
    for i in range (16):
        CATALOG_INFO_IMAGES_plot[i,:,:] = CATALOG_INFO_IMAGES_plot[i,:,:][np.argsort(CATALOG_INFO_IMAGES_plot[i,:, 6])]
        
    return CATALOG_INFO_IMAGES_plot

def CATALOG_INFO_IMAGES_CHANNELS_prep(CATALOG_INFO_IMAGES_CHANNELS,det_POSs):
    count_per_det                         = np.zeros(16)
    try:
        CATALOG_INFO_IMAGES_CHANNELS_shape    = CATALOG_INFO_IMAGES_CHANNELS.shape[0]
    except:
        print('Error: no info has been taken from the images')
    CATALOG_INFO_IMAGES_CHANNELS_shapeCOL = CATALOG_INFO_IMAGES_CHANNELS.shape[1]
    CATALOG_INFO_IMAGES_CHANNELS_plot     = np.empty((16,CATALOG_INFO_IMAGES_CHANNELS_shape,CATALOG_INFO_IMAGES_CHANNELS_shapeCOL))
    
    CATALOG_INFO_IMAGES_CHANNELS_plot[:]  = np.NaN
    
    for i in range (CATALOG_INFO_IMAGES_CHANNELS_shape):
        for count, det_POS in enumerate(det_POSs):
            if int(CATALOG_INFO_IMAGES_CHANNELS[i,2]) == det_POS:
                for j in range (CATALOG_INFO_IMAGES_CHANNELS_shapeCOL):
                    CATALOG_INFO_IMAGES_CHANNELS_plot[int(count),int(count_per_det[count]),j]  = CATALOG_INFO_IMAGES_CHANNELS[i,j]
                count_per_det[count]+=1   
                
    for i in range (16):
        CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,:] = CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,:][np.argsort(CATALOG_INFO_IMAGES_CHANNELS_plot[i,:, 1])]
        
    return CATALOG_INFO_IMAGES_CHANNELS_plot

def CATALOG_INFO_RAWLINES_prep(CATALOG_INFO_RAWLINES,det_POSs):
    count_per_det                         = np.zeros(16)
    CATALOG_INFO_RAWLINES_shape           = CATALOG_INFO_RAWLINES.shape[0]
    CATALOG_INFO_RAWLINES_shapeCOL        = CATALOG_INFO_RAWLINES.shape[1]
    CATALOG_INFO_RAWLINES_plot            = np.empty((16,CATALOG_INFO_RAWLINES_shape,CATALOG_INFO_RAWLINES_shapeCOL))
    
    CATALOG_INFO_RAWLINES_plot[:]         = np.NaN
    
    for i in range (CATALOG_INFO_RAWLINES_shape):
        for count, det_POS in enumerate(det_POSs):
            if int(CATALOG_INFO_RAWLINES[i,1]) == det_POS:
                for j in range (CATALOG_INFO_RAWLINES_shapeCOL):
                    CATALOG_INFO_RAWLINES_plot[int(count),int(count_per_det[count]),j]  = CATALOG_INFO_RAWLINES[i,j]
                count_per_det[count]+=1   
                
    for i in range (16):
        CATALOG_INFO_RAWLINES_plot[i,:,:] = CATALOG_INFO_RAWLINES_plot[i,:,:][np.argsort(CATALOG_INFO_RAWLINES_plot[i,:, 0])]
    
    
    return CATALOG_INFO_RAWLINES_plot

def check_time_images_HKTM(CATALOG_INFO_IMAGES, CATALOG_HK_PARAM, x_formatter, x_locator, today, seconds, t_min, t_max):
    T_START      = ([])
    PID          = ([])
    TIMEHK       = ([])
    SCE_POS      = ([])
    MODE         = ([])       
    T_OBS        = ([])
    T_date       = ([])

    for i in range (len(CATALOG_INFO_IMAGES)):
        #if int(CATALOG_INFO_IMAGES[i,4]) ==  32:                   # We choose a detector
        for j in range (len(CATALOG_HK_PARAM)):
            if CATALOG_HK_PARAM[j,1] == CATALOG_INFO_IMAGES[i,0]:
                T_START    = np.append( T_START, CATALOG_INFO_IMAGES[i,1])
                TIMEHK     = np.append(TIMEHK, CATALOG_HK_PARAM[j,0])
                T_OBS      = np.append(T_OBS, CATALOG_INFO_IMAGES[i,6])
    print('T_START, TIMEHK, T_OBS',T_START, TIMEHK, T_OBS)
                    
    plot_check_time_images_HKTM(T_START, TIMEHK, T_OBS, x_formatter, x_locator, today, seconds, t_min, t_max)
                    
    return

def plot_check_time_images_HKTM(T_START, TIMEHK, T_OBS, x_formatter, x_locator, today, seconds, t_min, t_max):
    
    fig = plt.figure(figsize=(15,15))

    fig.subplots_adjust(hspace=0, wspace=0)
    out_pdf = 'Output/Plots_TIME_IMAGE_VS_TIME_HK_{}_{}.pdf'.format(int(today.strftime("%d%m%Y")),seconds)
    
    pdf = matplotlib.backends.backend_pdf.PdfPages(out_pdf)
    ax = fig.add_subplot(2, 1, 1)
    #ax.axis('scaled')
    ax.scatter(T_OBS,TIMEHK) #,label='HK VS DATE_OBS (SPEC)')
    ax.plot(T_OBS,T_OBS,'k',label='Ratio Time_imaging/Time_telemetry = 1')
    
    ax.set_xlabel('Time_imaging',fontsize=14)
    ax.set_ylabel('Time_telemetry',fontsize=14)

    ax.xaxis.set_major_formatter(x_formatter)
    ax.xaxis.set_major_locator(x_locator)
    
    for tick in ax.get_xticklabels():
        tick.set_rotation(45)

    ax.yaxis.set_major_formatter(x_formatter)
    ax.yaxis.set_major_locator(x_locator)
    for tick in ax.get_yticklabels():
        tick.set_rotation(45)

    ax.yaxis.set_major_formatter(x_formatter)
    ax.yaxis.set_major_locator(x_locator)
    #ax.set_xlim(t_min, t_max)
    ax.legend()
    pdf.savefig(fig)
    pdf.close()
    
    return

def linear1Dinterp(x, a, b):
#def parabola(x):
    return a*x + b

def mysplit(s):
    head = s.rstrip('0123456789')
    tail = s[len(head):]
    return tail
    
def twoD_Gaussian (xdata_tuple, amplitude, x0, y0, sigma):
                    (x, y) = xdata_tuple   
                    g      = amplitude * np.exp(-((x-x0)**2+(y-y0)**2)/(2*sigma**2))  
                    return g.ravel()
                    
def twoD_Gaussian_2sigma (xdata_tuple, amplitude, x0, y0, sigmaX,sigmaY):
                    (x, y) = xdata_tuple    
                    g      = amplitude * np.exp(-((x-x0)**2/(2*sigmaX**2)+(y-y0)**2/(2*sigmaY**2)))  
                    return g.ravel()
            
def get_all_HK_param():
    param_list = {'0775':'NISP_DPUAG_5V_CPU_Current',
                    '4871':'NISP_DPUBG_5V_CPU_Current',
                    '0774':'NISP_DPUAG_3_3V_CPU_Current',
                    '4870':'NISP_DPUBG_3_3V_CPU_Current',
                    '0794':'NISP_DPUAG_CPU_Temperature',
                    '4890':'NISP_DPUBG_CPU_Temperature',
                    '0795':'NISP_DPUAG_MainDCDC_Temperature',
                    '4891':'NISP_DPUBG_MainDCDC_Temperature',
                    '0796':'NISP_DPUAG_DBB_Temperature',
                    '4892':'NISP_DPUBG_DBB_Temperature',
                    '0916':'NISP_DPUAG_DCU1_VDDA_V',
                    '1044':'NISP_DPUAG_DCU2_VDDA_V',
                    '1172':'NISP_DPUAG_DCU3_VDDA_V',
                    '1300':'NISP_DPUAG_DCU4_VDDA_V',
                    '1428':'NISP_DPUAG_DCU5_VDDA_V',
                    '1556':'NISP_DPUAG_DCU6_VDDA_V',
                    '1684':'NISP_DPUAG_DCU7_VDDA_V',
                    '1812':'NISP_DPUAG_DCU8_VDDA_V',
                    '5012':'NISP_DPUBG_DCU1_VDDA_V',
                    '5140':'NISP_DPUBG_DCU2_VDDA_V',
                    '5268':'NISP_DPUBG_DCU3_VDDA_V',
                    '5396':'NISP_DPUBG_DCU4_VDDA_V',
                    '5524':'NISP_DPUBG_DCU5_VDDA_V',
                    '5652':'NISP_DPUBG_DCU6_VDDA_V',
                    '5780':'NISP_DPUBG_DCU7_VDDA_V',
                    '5908':'NISP_DPUBG_DCU8_VDDA_V',
                    '0922':'NISP_DPUAG_DC1_VDDA_Rtrn',
                    '1050':'NISP_DPUAG_DC2_VDDA_Rtrn',
                    '1178':'NISP_DPUAG_DC3_VDDA_Rtrn',
                    '1306':'NISP_DPUAG_DC4_VDDA_Rtrn',
                    '1434':'NISP_DPUAG_DC5_VDDA_Rtrn',
                    '1562':'NISP_DPUAG_DC6_VDDA_Rtrn',
                    '1690':'NISP_DPUAG_DC7_VDDA_Rtrn',
                    '1818':'NISP_DPUAG_DC8_VDDA_Rtrn',
                    '5018':'NISP_DPUBG_DC1_VDDA_Rtrn',
                    '5146':'NISP_DPUBG_DC2_VDDA_Rtrn',
                    '5274':'NISP_DPUBG_DC3_VDDA_Rtrn',
                    '5402':'NISP_DPUBG_DC4_VDDA_Rtrn',
                    '5530':'NISP_DPUBG_DC5_VDDA_Rtrn',
                    '5658':'NISP_DPUBG_DC6_VDDA_Rtrn',
                    '5786':'NISP_DPUBG_DC7_VDDA_Rtrn',
                    '5914':'NISP_DPUBG_DC8_VDDA_Rtrn',
                    '0918':'NISP_DPUAG_DCU1_VREF_V',
                    '1046':'NISP_DPUAG_DCU2_VREF_V',
                    '1174':'NISP_DPUAG_DCU3_VREF_V',
                    '1302':'NISP_DPUAG_DCU4_VREF_V',
                    '1430':'NISP_DPUAG_DCU5_VREF_V',
                    '1558':'NISP_DPUAG_DCU6_VREF_V',
                    '1686':'NISP_DPUAG_DCU7_VREF_V',
                    '1814':'NISP_DPUAG_DCU8_VREF_V',
                    '5014':'NISP_DPUBG_DCU1_VREF_V',
                    '5142':'NISP_DPUBG_DCU2_VREF_V',
                    '5270':'NISP_DPUBG_DCU3_VREF_V',
                    '5398':'NISP_DPUBG_DCU4_VREF_V',
                    '5526':'NISP_DPUBG_DCU5_VREF_V',
                    '5654':'NISP_DPUBG_DCU6_VREF_V',
                    '5782':'NISP_DPUBG_DCU7_VREF_V',
                    '5910':'NISP_DPUBG_DCU8_VREF_V',
                    '0909':'NISP_DPUAG_DC1_V2P5D_V',
                    '1037':'NISP_DPUAG_DC2_V2P5D_V',
                    '1165':'NISP_DPUAG_DC3_V2P5D_V',
                    '1293':'NISP_DPUAG_DC4_V2P5D_V',
                    '1421':'NISP_DPUAG_DC5_V2P5D_V',
                    '1549':'NISP_DPUAG_DC6_V2P5D_V',
                    '1677':'NISP_DPUAG_DC7_V2P5D_V',
                    '1805':'NISP_DPUAG_DC8_V2P5D_V',
                    '5005':'NISP_DPUBG_DC1_V2P5D_V',
                    '5133':'NISP_DPUBG_DC2_V2P5D_V',
                    '5261':'NISP_DPUBG_DC3_V2P5D_V',
                    '5389':'NISP_DPUBG_DC4_V2P5D_V',
                    '5517':'NISP_DPUBG_DC5_V2P5D_V',
                    '5645':'NISP_DPUBG_DC6_V2P5D_V',
                    '5773':'NISP_DPUBG_DC7_V2P5D_V',
                    '5901':'NISP_DPUBG_DC8_V2P5D_V',
                    '0908':'NISP_DPUAG_DC1_V3P3D_V',
                    '1036':'NISP_DPUAG_DC2_V3P3D_V',
                    '1164':'NISP_DPUAG_DC3_V3P3D_V',
                    '1293':'NISP_DPUAG_DC4_V3P3D_V',
                    '1420':'NISP_DPUAG_DC5_V3P3D_V',
                    '1548':'NISP_DPUAG_DC6_V3P3D_V',
                    '1676':'NISP_DPUAG_DC7_V3P3D_V',
                    '1804':'NISP_DPUAG_DC8_V3P3D_V',
                    '5004':'NISP_DPUBG_DC1_V3P3D_V',
                    '5132':'NISP_DPUBG_DC2_V3P3D_V',
                    '5260':'NISP_DPUBG_DC3_V3P3D_V',
                    '5388':'NISP_DPUBG_DC4_V3P3D_V',
                    '5516':'NISP_DPUBG_DC5_V3P3D_V',
                    '5644':'NISP_DPUBG_DC6_V3P3D_V',
                    '5772':'NISP_DPUBG_DC7_V3P3D_V',
                    '5900':'NISP_DPUBG_DC8_V3P3D_V',
                    '0924':'NISP_DPUAG_DC1_SCE_Power',
                    '1052':'NISP_DPUAG_DC2_SCE_Power',
                    '1180':'NISP_DPUAG_DC3_SCE_Power',
                    '1308':'NISP_DPUAG_DC4_SCE_Power',
                    '1436':'NISP_DPUAG_DC5_SCE_Power',
                    '1564':'NISP_DPUAG_DC6_SCE_Power',
                    '1692':'NISP_DPUAG_DC7_SCE_Power',
                    '1820':'NISP_DPUAG_DC8_SCE_Power',
                    '5020':'NISP_DPUBG_DC1_SCE_Power',
                    '5148':'NISP_DPUBG_DC2_SCE_Power',
                    '5276':'NISP_DPUBG_DC3_SCE_Power',
                    '5404':'NISP_DPUBG_DC4_SCE_Power',
                    '5532':'NISP_DPUBG_DC5_SCE_Power',
                    '5660':'NISP_DPUBG_DC6_SCE_Power',
                    '5788':'NISP_DPUBG_DC7_SCE_Power',
                    '5916':'NISP_DPUBG_DC8_SCE_Power',
                    '0917':'NISP_DPUAG_DC1_VDDA_I',
                    '1045':'NISP_DPUAG_DC2_VDDA_I',
                    '1173':'NISP_DPUAG_DC3_VDDA_I',
                    '1301':'NISP_DPUAG_DC4_VDDA_I',
                    '1429':'NISP_DPUAG_DC5_VDDA_I',
                    '1557':'NISP_DPUAG_DC6_VDDA_I',
                    '1685':'NISP_DPUAG_DC7_VDDA_I',
                    '1813':'NISP_DPUAG_DC8_VDDA_I',
                    '5013':'NISP_DPUBG_DC1_VDDA_I',
                    '5141':'NISP_DPUBG_DC2_VDDA_I',
                    '5269':'NISP_DPUBG_DC3_VDDA_I',
                    '5397':'NISP_DPUBG_DC4_VDDA_I',
                    '5525':'NISP_DPUBG_DC5_VDDA_I',
                    '5653':'NISP_DPUBG_DC6_VDDA_I',
                    '5781':'NISP_DPUBG_DC7_VDDA_I',
                    '5909':'NISP_DPUBG_DC8_VDDA_I',
                    '0919':'NISP_DPUAG_DC1_VREF_I',
                    '1047':'NISP_DPUAG_DC2_VREF_I',
                    '1175':'NISP_DPUAG_DC3_VREF_I',
                    '1303':'NISP_DPUAG_DC4_VREF_I',
                    '1431':'NISP_DPUAG_DC5_VREF_I',
                    '1559':'NISP_DPUAG_DC6_VREF_I',
                    '1687':'NISP_DPUAG_DC7_VREF_I',
                    '1815':'NISP_DPUAG_DC8_VREF_I',
                    '5015':'NISP_DPUBG_DC1_VREF_I',
                    '5143':'NISP_DPUBG_DC2_VREF_I',
                    '5271':'NISP_DPUBG_DC3_VREF_I',
                    '5399':'NISP_DPUBG_DC4_VREF_I',
                    '5527':'NISP_DPUBG_DC5_VREF_I',
                    '5655':'NISP_DPUBG_DC6_VREF_I',
                    '5783':'NISP_DPUBG_DC7_VREF_I',
                    '5911':'NISP_DPUBG_DC8_VREF_I',
                    '0912':'NISP_DPUAG_DC1_V3P3_I',
                    '1040':'NISP_DPUAG_DC2_V3P3_I',
                    '1168':'NISP_DPUAG_DC3_V3P3_I',
                    '1296':'NISP_DPUAG_DC4_V3P3_I',
                    '1424':'NISP_DPUAG_DC5_V3P3_I',
                    '1552':'NISP_DPUAG_DC6_V3P3_I',
                    '1680':'NISP_DPUAG_DC7_V3P3_I',
                    '1808':'NISP_DPUAG_DC8_V3P3_I',
                    '5008':'NISP_DPUBG_DC1_V3P3_I',
                    '5136':'NISP_DPUBG_DC2_V3P3_I',
                    '5264':'NISP_DPUBG_DC3_V3P3_I',
                    '5392':'NISP_DPUBG_DC4_V3P3_I',
                    '5520':'NISP_DPUBG_DC5_V3P3_I',
                    '5648':'NISP_DPUBG_DC6_V3P3_I',
                    '5776':'NISP_DPUBG_DC7_V3P3_I',
                    '5904':'NISP_DPUBG_DC8_V3P3_I',
                    '0913':'NISP_DPUA_DC1_V2P5D_I',
                    '1041':'NISP_DPUA_DC2_V2P5D_I',
                    '1169':'NISP_DPUA_DC3_V2P5D_I',
                    '1297':'NISP_DPUA_DC4_V2P5D_I',
                    '1425':'NISP_DPUA_DC5_V2P5D_I',
                    '1553':'NISP_DPUA_DC6_V2P5D_I',
                    '1681':'NISP_DPUA_DC7_V2P5D_I',
                    '1809':'NISP_DPUA_DC8_V2P5D_I',
                    '5009':'NISP_DPUB_DC1_V2P5D_I',
                    '5137':'NISP_DPUB_DC2_V2P5D_I',
                    '5265':'NISP_DPUB_DC3_V2P5D_I',
                    '5393':'NISP_DPUB_DC4_V2P5D_I',
                    '5521':'NISP_DPUB_DC5_V2P5D_I',
                    '5649':'NISP_DPUB_DC6_V2P5D_I',
                    '5777':'NISP_DPUB_DC7_V2P5D_I',
                    '5905':'NISP_DPUB_DC8_V2P5D_I',
                    '1940':'NISP_DPUAG_SC1_SidecarTS',
                    '2068':'NISP_DPUAG_SC2_SidecarTS',
                    '2196':'NISP_DPUAG_SC3_SidecarTS',
                    '2324':'NISP_DPUAG_SC4_SidecarTS',
                    '2452':'NISP_DPUAG_SC5_SidecarTS',
                    '2580':'NISP_DPUAG_SC6_SidecarTS',
                    '2708':'NISP_DPUAG_SC7_SidecarTS',
                    '2836':'NISP_DPUAG_SC8_SidecarTS',
                    '6036':'NISP_DPUBG_SC1_SidecarTS',
                    '6164':'NISP_DPUBG_SC2_SidecarTS',
                    '6292':'NISP_DPUBG_SC3_SidecarTS',
                    '6420':'NISP_DPUBG_SC4_SidecarTS',
                    '6548':'NISP_DPUBG_SC5_SidecarTS',
                    '6676':'NISP_DPUBG_SC6_SidecarTS',
                    '6804':'NISP_DPUBG_SC7_SidecarTS',
                    '6932':'NISP_DPUBG_SC8_SidecarTS',
                    '1928':'NISP_DPUAG_SC1_VDDA_Vtge',
                    '2056':'NISP_DPUAG_SC2_VDDA_Vtge',
                    '2184':'NISP_DPUAG_SC3_VDDA_Vtge',
                    '2312':'NISP_DPUAG_SC4_VDDA_Vtge',
                    '2440':'NISP_DPUAG_SC5_VDDA_Vtge',
                    '2568':'NISP_DPUAG_SC6_VDDA_Vtge',
                    '2696':'NISP_DPUAG_SC7_VDDA_Vtge',
                    '2824':'NISP_DPUAG_SC8_VDDA_Vtge',
                    '6024':'NISP_DPUBG_SC1_VDDA_Vtge',
                    '6152':'NISP_DPUBG_SC2_VDDA_Vtge',
                    '6280':'NISP_DPUBG_SC3_VDDA_Vtge',
                    '6408':'NISP_DPUBG_SC4_VDDA_Vtge',
                    '6536':'NISP_DPUBG_SC5_VDDA_Vtge',
                    '6664':'NISP_DPUBG_SC6_VDDA_Vtge',
                    '6792':'NISP_DPUBG_SC7_VDDA_Vtge',
                    '6920':'NISP_DPUBG_SC8_VDDA_Vtge',
                    '1922':'NISP_DPUAG_SC1_VResetVtg',
                    '2050':'NISP_DPUAG_SC2_VResetVtg',
                    '2178':'NISP_DPUAG_SC3_VResetVtg',
                    '2306':'NISP_DPUAG_SC4_VResetVtg',
                    '2434':'NISP_DPUAG_SC5_VResetVtg',
                    '2562':'NISP_DPUAG_SC6_VResetVtg',
                    '2690':'NISP_DPUAG_SC7_VResetVtg',
                    '2818':'NISP_DPUAG_SC8_VResetVtg',
                    '6018':'NISP_DPUBG_SC1_VResetVtg',
                    '6146':'NISP_DPUBG_SC2_VResetVtg',
                    '6724':'NISP_DPUBG_SC3_VResetVtg',
                    '6402':'NISP_DPUBG_SC4_VResetVtg',
                    '6530':'NISP_DPUBG_SC5_VResetVtg',
                    '6658':'NISP_DPUBG_SC6_VResetVtg',
                    '6786':'NISP_DPUBG_SC7_VResetVtg',
                    '6914':'NISP_DPUBG_SC8_VResetVtg',
                    '1923':'NISP_DPUA_SC1_DSubVtge',
                    '2051':'NISP_DPUA_SC2_DSubVtge',
                    '2179':'NISP_DPUA_SC3_DSubVtge',
                    '2307':'NISP_DPUA_SC4_DSubVtge',
                    '2435':'NISP_DPUA_SC5_DSubVtge',
                    '2563':'NISP_DPUA_SC6_DSubVtge',
                    '2691':'NISP_DPUA_SC7_DSubVtge',
                    '2819':'NISP_DPUA_SC8_DSubVtge',
                    '6019':'NISP_DPUB_SC1_DSubVtge',
                    '6147':'NISP_DPUB_SC2_DSubVtge',
                    '6275':'NISP_DPUB_SC3_DSubVtge',
                    '6403':'NISP_DPUB_SC4_DSubVtge',
                    '6531':'NISP_DPUB_SC5_DSubVtge',
                    '6659':'NISP_DPUB_SC6_DSubVtge',
                    '6787':'NISP_DPUB_SC7_DSubVtge',
                    '6915':'NISP_DPUB_SC8_DSubVtge',
                    '1938':'NISP_DPUAG_SC1_H2TS33int',
                    '2066':'NISP_DPUAG_SC2_H2TS33int',
                    '2194':'NISP_DPUAG_SC3_H2TS33int',
                    '2322':'NISP_DPUAG_SC4_H2TS33int',
                    '2450':'NISP_DPUAG_SC5_H2TS33int',
                    '2578':'NISP_DPUAG_SC6_H2TS33int',
                    '2706':'NISP_DPUAG_SC7_H2TS33int',
                    '2834':'NISP_DPUAG_SC8_H2TS33int',
                    '6034':'NISP_DPUBG_SC1_H2TS33int',
                    '6162':'NISP_DPUBG_SC2_H2TS33int',
                    '6290':'NISP_DPUBG_SC3_H2TS33int',
                    '6418':'NISP_DPUBG_SC4_H2TS33int',
                    '6546':'NISP_DPUBG_SC5_H2TS33int',
                    '6674':'NISP_DPUBG_SC6_H2TS33int',
                    '6802':'NISP_DPUBG_SC7_H2TS33int',
                    '6930':'NISP_DPUBG_SC8_H2TS33int',
                    '0619':'NISP_OMATC_CSS1_CryoTemp CSS-cryo',
                    '0620':'NISP_OMATC_CSS2_CryoTemp CSS-cryo',
                    '0621':'NISP_OMATC_SSS1_CryoTemp CSS-cryo',
                    '0622':'NISP_OMATC_SSS2_CryoTemp CSS-cryo',
                    '0643':'NISP_DPUAG_LSQ_FitDeltaT',
                    '4739':'NISP_DPUBG_LSQ_FitDeltaT',
                    '0644':'NISP_DPUAG_Dat&X2_DeltaT',
                    '4740':'NISP_DPUBG_Dat&X2_DeltaT',
                    '0645':'NISP_DPUAG_DataComprFact',
                    '4741':'NISP_DPUBG_DataComprFact',
                    '0646':'NISP_DPUAG_Chi2ComprFact',
                    '4742':'NISP_DPUBG_Chi2ComprFact',
                    '3975':'NISP_DPUAG_DRBcorEdaErCt',
                    '8071':'NISP_DPUBG_DRBcorEdaErCt',
                    '3974':'NISP_DPUAG_DRBmltEdaErCt',
                    '8070':'NISP_DPUBG_DRBmltEdaErCt',
                    '3987':'NISP_DPUAG_DRB_SpW_ErrCt',
                    '8083':'NISP_DPUBG_DRB_SpW_ErrCt',
                    '3970':'NISP_DPUAG_DBBnCrScrErCt',
                    '8066':'NISP_DPUBG_DBBnCrScrErCt',
                    '3971':'NISP_DPUAG_DBBcorScrErCt',
                    '8067':'NISP_DPUBG_DBBcorScrErCt',
                    '3972':'NISP_DPUAG_DBBmltEdaErCt',
                    '8068':'NISP_DPUBG_DBBmltEdaErCt',
                    '3973':'NISP_DPUAG_DBBcorEdaErCt',
                    '8069':'NISP_DPUBG_DBBcorEdaErCt',
                    '3976':'NISP_DPUAG_DCU_SpW1_ErCt',
                    '3977':'NISP_DPUAG_DCU_SpW2_ErCt',
                    '3978':'NISP_DPUAG_DCU_SpW3_ErCt',
                    '3979':'NISP_DPUAG_DCU_SpW4_ErCt',
                    '3980':'NISP_DPUAG_DCU_SpW5_ErCt',
                    '3981':'NISP_DPUAG_DCU_SpW6_ErCt',
                    '3982':'NISP_DPUAG_DCU_SpW7_ErCt',
                    '3983':'NISP_DPUAG_DCU_SpW8_ErCt',
                    '8072':'NISP_DPUBG_DCU_SpW1_ErCt',
                    '8073':'NISP_DPUBG_DCU_SpW2_ErCt',
                    '8074':'NISP_DPUBG_DCU_SpW3_ErCt',
                    '8075':'NISP_DPUBG_DCU_SpW4_ErCt',
                    '8076':'NISP_DPUBG_DCU_SpW5_ErCt',
                    '8077':'NISP_DPUBG_DCU_SpW6_ErCt',
                    '8078':'NISP_DPUBG_DCU_SpW7_ErCt',
                    '8079':'NISP_DPUBG_DCU_SpW8_ErCt',
                    '0620':'NISP_OMATC_CSS2_CryoTemp',
                    '0621':'NISP_OMATC_SSS1_CryoTemp',
                    '0622':'NISP_OMATC_SSS2_CryoTemp',
                    '0623':'NISP_OMATC_FWA_CryoTemp',
                    '0624':'NISP_OMATC_GWA_CryoTemp',
                    '0625':'NISP_OMATC_SA1_FRngTemp',
                    '0626':'NISP_OMATC_SA2_FRngTemp',
                    '0627':'NISP_OMATC_SA3_FRngTemp',
                    '0628':'NISP_OMATC_SA4_FRngTemp',
                    '0629':'NISP_OMATC_SA5_FRngTemp',
                    '0630':'NISP_OMATC_SA6_FRngTemp',
                    '0631':'NISP_OMATC_CSS1_FRngTemp',
                    '0632':'NISP_OMATC_CSS2_FRngTemp',
                    '0633':'NISP_OMATC_SSS1_FRngTemp',
                    '0634':'NISP_OMATC_SSS2_FRngTemp',
                    '0635':'NISP_OMATC_FWA_FRngTemp',
                    '0636':'NISP_OMATC_GWA_FRngTemp',
                    '0102':'ICU_LVPS_Temp',
                    '0101':'ICU-CDPU_Temp',
                    '0103':'ICU 5V Voltage',
                    '0104':'ICU 5V Current',
                    '0485':'OMACU_LED_1_Voltage',
                    '0486':'OMACU_LED_2_Voltage',
                    '0487':'OMACU_LED_3_Voltage',
                    '0488':'OMACU_LED_4_Voltage',
                    '0489':'OMACU_LED_5_Voltage'}
    
    return param_list
