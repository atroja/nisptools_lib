# setup.py


from setuptools import find_packages, setup

setup (
		name='nisptools',
		version='0.6.2',
		description='Python lib including routines for NISP data analysis',
		author='Antonino Troja',
		author_email='antonino.troja@pd.infn.it',
		license='MIT',
		classifiers=[
			'Development Status :: 3 - Alpha',
			'Prigramming Language :: Python :: 3',
		],
		keywords='nisp euclid cosmology',
		packages=find_packages(include=['nisptools']),
		install_requires=['numpy', 'astropy', 'matplotlib', 'datetime'],
		python_requires='>=3',
		setup_requires=['pytest-runner'],
		tests_require=['pytest'],
		test_suite='tests',
)
