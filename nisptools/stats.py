import numpy as np
import numpy as np
from astropy.io import fits
import os
from nisptools.detectors import Detectors
from nisptools.io import read_acquisition, write_focalplane_to_fits, read_acquisition_fnames

#*****************************
#                            *
#    NO OFFSET NO SCALING    *
#                            *
#*****************************

def summary_stats (science):
                               
    good_pixel = np.where (science != 0)
    good_science = science [good_pixel [0], good_pixel [1]]
    mean = np.mean(good_science)
    variance = np.var(good_science)
    median = np.median(good_science)
               
    return mean, variance, median

def summary_stats_1D (science):
                               
    good_pixel = np.where (science != 0)
    good_science = science [good_pixel]
    mean = np.mean(good_science)
    variance = np.var(good_science)
    median = np.median(good_science)
               
    return mean, variance, median

def Science_stats (dirpath, fname1_array, badpix, outfname_stem, NPIXX = 2048, NPIXY = 2048, headernames = ['SCI'], selectarea = None, chi2_threshold = 47):
    
    print ("Science_stats START")
    
    dets = Detectors ()
    
    area     = np.zeros ([NPIXX, NPIXY], dtype = bool)
    if selectarea:
        if len(selectarea) == 4:
            area [selectarea[0]:selectarea[1], selectarea[2]:selectarea[3]] = True
        if len(selectarea) == 2:
            area [:selectarea[0], :] = True
            area [-selectarea[0]:, :] = True
            area [:, :selectarea[1]] = True
            area [:, -selectarea[1]:] = True
    else:
        area [:,:] = True
        
    count_map = np.empty ([NPIXX,NPIXY])    

    headerout = []
    
    badpix_pos = []
    for n in range (16):
        badpix_pos.append (np.where (badpix [n] == 1))
        
    nmodes = len (headernames)
    count = np.zeros ([nmodes,16], dtype = int) 
    
    n=0
    for fname in fname1_array:   

        print (n+1, "of", len (fname1_array))

        headerin, science, chi2 = read_acquisition (dirpath + "/" + fname)
        
        det = dets.det2indx [headerin [2]] - 1

        mod = -1
        for hmod in range (nmodes):
            if headerin [3] == headernames [hmod]:
                mod = hmod
                check = 1
                break
        if mod == -1:
            n += 1
            continue
            
        saturation_pos = np.where (science == 65535)
        if np.max (chi2) > 1:
            chi2high_pos = np.where (chi2 > chi2_threshold)
        else:
            chi2high_pos = np.where (chi2 > 0.9)
            
        chi2_over = len (chi2high_pos[0])
            
        count_map.fill (1.)
        count_map [saturation_pos[0], saturation_pos[1]] = 0.
        count_map [chi2high_pos[0], chi2high_pos[1]] = 0.
        count_map [badpix_pos [det][0], badpix_pos [det][1]] = 0.
        
        science = science * count_map
        
        count [mod][det] += 1
        
        if selectarea:
            mean, variance, median = summary_stats_1D (science [area])
        else:
            mean, variance, median = summary_stats (science)
        
        headerin.append (mean)
        headerin.append (variance)
        headerin.append (median)
        headerin.append (chi2_over)
        headerout.append (headerin)

        n += 1
    
    for hmod in range (nmodes):
        print ("{} acquisition".format (headernames [hmod]))
        for det in range (16):
            if (count [hmod][det] == 0):
                continue
            print ("Detector {}: {}".format(dets.indx2det[det+1], count [hmod][det]))
               
    # ORDER: fname, time, det, mod, macc, pid, mean, variance, median
    #header_final = np.array ([headerout [0][i] for i in range (len (headerout [0]))], dtype = object)
    #for n in range (1,len (headerout)):
    #    header_final_tmp = np.array ([headerout [n][i] for i in range (len (headerout [n]))], dtype = object)
    #    header_final = np.append (header_final, header_final_tmp)
    #header_final = header_final.reshape (len (headerout),9)
    header_final = np.array (#[headerout [0][i] for i in range (len (headerout [0]))], 
                         [(headerout [0][0],
                           headerout [0][1],
                           headerout [0][2],
                           headerout [0][3],
                           headerout [0][4],
                           headerout [0][5],
                           headerout [0][6],
                           headerout [0][7],
                           headerout [0][8],
                           headerout [0][9], 
                           headerout [0][10], 
                           headerout [n][10])],
                                 dtype = [('fname', '<U100'),
                                 ('date', '<U50'),
                                 ('det', '<U10'),
                                 ('extname', '<U10'),
                                 ('ngroups', '<i4'),
                                 ('nframes', '<i4'),
                                 ('ndrops', '<i4'),
                                 ('pid', '<i4'),
                                 ('mean', '<f8'),
                                 ('var', '<f8'),
                                 ('median', '<f8'),
                                 ('chi2_over', '<i8')])
    for n in range (1,len (headerout)):
        header_final_tmp = np.array (#[headerout [n][i] for i in range (len (headerout [n]))], 
                         [(headerout [n][0],
                           headerout [n][1],
                           headerout [n][2],
                           headerout [n][3],
                           headerout [n][4],
                           headerout [n][5],
                           headerout [n][6],
                           headerout [n][7],
                           headerout [n][8],
                           headerout [n][9], 
                           headerout [n][10], 
                           headerout [n][11])],
                                 dtype = [('fname', '<U100'),
                                 ('date', '<U50'),
                                 ('det', '<U10'),
                                 ('extname', '<U10'),
                                 ('ngroups', '<i4'),
                                 ('nframes', '<i4'),
                                 ('ndrops', '<i4'),
                                 ('pid', '<i4'),
                                 ('mean', '<f8'),
                                 ('var', '<f8'),
                                 ('median', '<f8'),
                                 ('chi2_over', '<i8')])
        header_final = np.append (header_final, header_final_tmp)
               
    print ("Writing {}".format (outfname_stem + "_SummaryStatistics_per_Acquisition.npz"))
    np.savez (outfname_stem + "_SummaryStatistics_per_Acquisition.npz",
              FNAME    = header_final ['fname'],
              DATEOBS  = header_final ['date'],
              DETID    = header_final ['det'],
              MOD      = header_final ['extname'],
              NGROUPS  = header_final ['ngroups'],
              NFRAMES  = header_final ['nframes'],
              NDROPS   = header_final ['ndrops'],
              PID      = header_final ['pid'],
              MEAN     = header_final ['mean'],
              VARIANCE = header_final ['var'],
              MEDIAN   = header_final ['median'],
              CHI2_OVER= header_final ['chi2_over'])
    print ("Done")

    print ("Science_stats DONE")

def Darks_sum (dirpath, fname1_array, badpix, outfname_stem, NPIXX = 2048, NPIXY = 2048, headernames = ['PENGDARK', 'SENGDARK'], selectarea = None, chi2dist = False, chi2_threshold = 47):
    
    print ("Darks_sum START")

    dets = Detectors ()
    
    headerout = []
    
    badpix_pos = []
    for n in range (16):
        badpix_pos.append (np.where (badpix [n] == 1))
    
    nmodes   = len (headernames)
    Ex       = np.zeros ([nmodes, 16, NPIXX, NPIXY])
    Ex2      = np.zeros ([nmodes, 16, NPIXX, NPIXY])
    pixcount = np.zeros ([nmodes, 16, NPIXX, NPIXY])
    count    = np.zeros ([nmodes, 16], dtype = int) 
    if chi2dist:
        Exchi2 = np.zeros ([nmodes, 16, NPIXX, NPIXY])

    
    area     = np.zeros ([NPIXX, NPIXY], dtype = bool)
    if selectarea:
        if len(selectarea) == 4:
            area [selectarea[0]:selectarea[1], selectarea[2]:selectarea[3]] = True
        if len(selectarea) == 2:
            area [:selectarea[0], :] = True
            area [-selectarea[0]:, :] = True
            area [:, :selectarea[1]] = True
            area [:, -selectarea[1]:] = True
    else:
        area [:,:] = True
        
    count_map = np.empty ([NPIXX,NPIXY])    
    
    n = 0
    for fname in fname1_array:   
        
        print (n+1, "of", len (fname1_array))
        
        headerin, science, chi2 = read_acquisition (dirpath + "/" + fname)
                
        det = dets.det2indx [headerin [2]] - 1
        
        mod = -1
        for hmod in range (nmodes):
            if headerin [3] == headernames [hmod]:
                mod = hmod
                break
        if mod == -1:
            n += 1
            continue
 
        saturation_pos = np.where (science == 65535)
        if np.max (chi2) > 1:
            chi2high_pos = np.where (chi2 > chi2_threshold)
        else:
            chi2high_pos = np.where (chi2 > 0.9)
            
        chi2_over = len (chi2high_pos[0])
        if chi2dist:
            Exchi2 [mod][det]  = Exchi2 [mod][det] + chi2
            
        count_map.fill (1.)
        count_map [saturation_pos[0], saturation_pos[1]] = 0.
        count_map [chi2high_pos[0], chi2high_pos[1]] = 0.
        count_map [badpix_pos [det][0], badpix_pos [det][1]] = 0.
        
        science = science * count_map

        pixcount [mod][det] = pixcount [mod][det] + count_map
        Ex [mod][det]  = Ex [mod][det]  + science
        Ex2 [mod][det] = Ex2 [mod][det] + science**2
        
        count [mod][det] += 1
        
        if selectarea:
            mean, variance, median = summary_stats_1D (science [area])
        else:
            mean, variance, median = summary_stats (science)
        
        headerin.append (mean)
        headerin.append (variance)
        headerin.append (median)
        headerin.append (chi2_over)
        headerout.append (headerin)

        n += 1
    
    for hmod in range (nmodes):
        print ("{} acquisition".format (headernames [hmod]))
        for det in range (16):
            if (count [hmod][det] == 0):
                continue
            if chi2dist:
                Exchi2 [hmod][det] = Exchi2 [hmod][det] / count [hmod][det]
            print ("Detector {}: {}".format(dets.indx2det[det+1], count [hmod][det]))
               
    # ORDER: fname, time, det, mod, macc, pid, mean, variance, median
    #header_final = np.array ([headerout [0][i] for i in range (len (headerout [0]))], dtype = object)
    #for n in range (1,len (headerout)):
    #    header_final_tmp = np.array ([headerout [n][i] for i in range (len (headerout [n]))], dtype = object)
    #    header_final = np.append (header_final, header_final_tmp)
    #header_final = header_final.reshape (len (headerout),9)
    header_final = np.array (#[headerout [0][i] for i in range (len (headerout [0]))], 
                         [(headerout [0][0],
                          headerout [0][1],
                          headerout [0][2],
                          headerout [0][3],
                          headerout [0][4],
                          headerout [0][5],
                          headerout [0][6],
                          headerout [0][7],
                          headerout [0][8],
                          headerout [0][9], 
                          headerout [0][10], 
                          headerout [0][11])],
                                 dtype = [('fname', '<U100'),
                                 ('date', '<U50'),
                                 ('det', '<U10'),
                                 ('extname', '<U10'),
                                 ('ngroups', '<i4'),
                                 ('nframes', '<i4'),
                                 ('ndrops', '<i4'),
                                 ('pid', '<i4'),
                                 ('mean', '<f8'),
                                 ('var', '<f8'),
                                 ('median', '<f8'),
                                 ('chi2_over', '<i8')])
    for n in range (1,len (headerout)):
        header_final_tmp = np.array (#[headerout [n][i] for i in range (len (headerout [n]))], 
                         [(headerout [n][0],
                           headerout [n][1],
                           headerout [n][2],
                           headerout [n][3],
                           headerout [n][4],
                           headerout [n][5],
                           headerout [n][6],
                           headerout [n][7],
                           headerout [n][8],
                           headerout [n][9], 
                           headerout [n][10], 
                           headerout [n][11])],
                                 dtype = [('fname', '<U100'),
                                 ('date', '<U50'),
                                 ('det', '<U10'),
                                 ('extname', '<U10'),
                                 ('ngroups', '<i4'),
                                 ('nframes', '<i4'),
                                 ('ndrops', '<i4'),
                                 ('pid', '<i4'),
                                 ('mean', '<f8'),
                                 ('var', '<f8'),
                                 ('median', '<f8'),
                                 ('chi2_over', '<i8')])
        header_final = np.append (header_final, header_final_tmp)
               
    print ("Writing {}".format (outfname_stem + "_SummaryStatistics_per_Acquisition.npz"))
    np.savez (outfname_stem + "_SummaryStatistics_per_Acquisition.npz",
              FNAME    = header_final ['fname'],
              DATEOBS  = header_final ['date'],
              DETID    = header_final ['det'],
              MOD      = header_final ['extname'],
              NGROUPS  = header_final ['ngroups'],
              NFRAMES  = header_final ['nframes'],
              NDROPS   = header_final ['ndrops'],
              PID      = header_final ['pid'],
              MEAN     = header_final ['mean'],
              VARIANCE = header_final ['var'],
              MEDIAN   = header_final ['median'],
              CHI2_OVER= header_final ['chi2_over'])
    print ("Done")
    
    print ("Writing {}".format (outfname_stem + "_PixCount.fits"))
    write_focalplane_to_fits (outfname_stem + "_PixCount.fits", pixcount, mode = headernames)
    print ("Done")
               
    print ("Writing {}".format (outfname_stem + "_Ex.fits"))
    write_focalplane_to_fits (outfname_stem + "_Ex.fits", Ex, mode = headernames)
    print ("Done")
               
    print ("Writing {}".format (outfname_stem + "_Ex2.fits"))
    write_focalplane_to_fits (outfname_stem + "_Ex2.fits", Ex2, mode = headernames)
    print ("Done")
    
    if chi2dist:
        print ("Writing {}".format (outfname_stem + "_ExCHI2.fits"))
        write_focalplane_to_fits (outfname_stem + "_CHI2AV.fits", Exchi2, mode = headernames)
        print ("Done")

    print ("Darks_sum DONE")
        
def Darks_stats (Ex, Ex2, pixcount, outfname_stem, NPIXX=2048, NPIXY=2048, headernames = ['PENGDARK{:1d}{:1d}', 'SENGDARK{:2d}{:1d}']):

    print ("Darks_stats START")
    dets = Detectors ()
    
    nmodes = len (headernames)
    average = np.zeros ([nmodes,16, NPIXX, NPIXY], dtype = float)
    variance =  np.zeros ([nmodes,16, NPIXX, NPIXY], dtype = float)
    
    for m in range (nmodes):
        for n in range (16):
            k = headernames[m].format (int(dets.indx2det[n+1][0]),int(dets.indx2det[n+1][1]))
            
            pixNAN = np.where (pixcount [k].data > 1)
               
            average [m][n][pixNAN[0], pixNAN[1]] = Ex [k].data[pixNAN[0], pixNAN[1]] / pixcount [k].data[pixNAN[0], pixNAN[1]]
               
            variance [m][n][pixNAN[0], pixNAN[1]]  = (Ex2 [k].data[pixNAN[0], pixNAN[1]] - (Ex [k].data[pixNAN[0], pixNAN[1]]**2)/pixcount [k].data[pixNAN[0], pixNAN[1]] ) / (pixcount [k].data[pixNAN[0], pixNAN[1]] -1.)
            
            pixNAN = np.where (pixcount [k].data <= 1)
            average [m][n][pixNAN[0], pixNAN[1]] = 0.
            variance [m][n][pixNAN[0], pixNAN[1]] = 0.
            
            #average [m][n] = (average [m][n]-off)*scale[m]
            #stddev [m][n] = np.sqrt(stddev [m][n]) * scale[m]

    print ("Writing {}".format (outfname_stem + "_Average.fits"))
    write_focalplane_to_fits (outfname_stem + "_Average.fits", average, mode = headernames)
    print ("Done")
    print ("Writing {}".format (outfname_stem + "_Variance.fits"))
    write_focalplane_to_fits (outfname_stem + "_Variance.fits", variance, mode = headernames)
    print ("Done")

    print ("Darks_stats DONE")        
    
def Frame_spatial_median (frame):
    
    det = Detectors ()
    
    frame_red = []
    for n in range (16):
        
        try:
            nonzero = np.where  (frame [n]!=0)
            
            frame_red.append([ det.indx2det_int[n+1],
                           np.median         (frame [n][nonzero [0], nonzero [1]]),
                           np.percentile     (frame [n][nonzero [0], nonzero [1]], 25),
                           np.percentile     (frame [n][nonzero [0], nonzero [1]],75)])
        except:
            frame_red.append([ det.indx2det_int[n+1], -1, -1, -1 ])

    return np.array (frame_red)

def get_baseline (baseline_path, last_acq=10000, first_acq=1, ref_pix = False, headername = 'SCIENCE', pid=[-1], acq_type=None, MACC=None):    
    
    dets = Detectors ()
    
    dirpath = os.path.split(baseline_path)[0]+"/"
    fnames = read_acquisition_fnames (baseline_path, last_acq, pid=pid, acq_type=acq_type, MACC=MACC)
    
    #print (fnames)
    
    baseline = np.ones ([16,last_acq-first_acq+1])*(-1)
    
    print (np.shape(baseline))
    
    frame = np.ones ((2048,2048), dtype = bool)
    if not ref_pix:
        frame [4:-4, 4:-4] = False
    
    for f in fnames:
        
        try:
            
            if int (f [-15:-11]) < first_acq:
                continue
            
            tmp = fits.open (dirpath+f)
            det_in = tmp[0].header ['SCE_POS']
            #grp = tmp[0].header ['GRP_CNT']
            #frm = tmp[0].header ['FRAMEGRP']
            #drp = tmp[0].header ['DROPS']
            science = tmp [headername].data[frame]
            tmp.close ()
            
            det = dets.det2indx [det_in] - 1
            acq =  int (f [-15:-11]) - first_acq
            
            baseline [det][acq] = np.median (science)
        except:
            continue
            
    return baseline