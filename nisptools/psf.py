"""
We define in this script some functions to be used for the PSF search and corresponding seed pixel region identification.

"""

#import os
import numpy as np
import matplotlib.pyplot as plt
import fnmatch
#from matplotlib import cm
import matplotlib.backends.backend_pdf
import scipy.optimize as opt
import pandas as pd
from mpl_toolkits import mplot3d
import time
from datetime import date
from astropy.io import fits
from scipy.stats import gaussian_kde

def mysplit(s):
    head = s.rstrip('0123456789')
    tail = s[len(head):]
    return tail
    
def twoD_Gaussian (xdata_tuple, amplitude, x0, y0, sigma):
                    (x, y) = xdata_tuple   
                    g = amplitude * np.exp(-((x-x0)**2+(y-y0)**2)/(2*sigma**2))  
                    return g.ravel()
                    
def twoD_Gaussian_2sigma (xdata_tuple, amplitude, x0, y0, sigmaX,sigmaY):
                    (x, y) = xdata_tuple    
                    g = amplitude * np.exp(-((x-x0)**2/(2*sigmaX**2)+(y-y0)**2/(2*sigmaY**2)))  
                    return g.ravel()

def get_bad_pixels_SCI(path_bad_pixels):
    DET_IDs      = ['453-024-019.MAPS'
                    ,'272-042-024.MAPS','632-085-017.MAPS'
                    ,'267-084-043.MAPS','268-045-019.MAPS','285-064-024.MAPS'
                    ,'548-071-025.MAPS', '452-054-043.MAPS','280-114-056.MAPS'
                    ,'284-081-017.MAPS','278-015-043.MAPS','269-072-056.MAPS'
                    ,'458-103-025.MAPS','249-014-015.MAPS', '221-094-056.MAPS'
                    ,'628-052-025.MAPS']
    BAD_PIX_MAPS = np.zeros((16,2040,2040))
    for count, DET_ID in enumerate(DET_IDs):
        BAD_PIX_MAP              = fits.open(path_bad_pixels+DET_ID)
        BAD_PIX_MAPS[count,:,:]  = BAD_PIX_MAP['dead_pixels'].data[4:-4,4:-4].astype(int)
    return BAD_PIX_MAPS


def get_Image_SCI(path_dark):
    IMAGE                   = fits.open(path_dark)
    IMAGE_SCI               = IMAGE['SCIENCE'].data[4:-4,4:-4].astype(int)
    return IMAGE_SCI

def subtraction_dark_badpx_satpx(path_files,file_name,DARK_SCI,BAD_PIX_MAPS):
    FITS_IMAGE              = fits.open((path_files + str(str(file_name)[1:-1])[1:-1]).replace("'",""))#str(file_name)[1:-1])
    SCI                     = FITS_IMAGE['SCIENCE'].data[4:-4,4:-4].astype(int)   #[1830:1855,155:180].astype(int)   # Signal in ADU/px
    CHI2                    = FITS_IMAGE['CHI2'].data[4:-4,4:-4].astype(int)
    
    DISC_PX_MAP             = np.zeros((2040,2040)) 
    det_POSs                = [11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44]
    
    PID                     = FITS_IMAGE[0].header['PID1'] * 65536 + FITS_IMAGE[0].header['PID2']
    
    ## Saturated pixels
    #  Identification of the saturated pixels and filling up the map of discarded pixel
    Saturated_pixels = np.where(SCI == 65535)
    DISC_PX_MAP[Saturated_pixels[0], Saturated_pixels[1]] = 1
    
    ## CHI2 = 1 identification
    CHI2_1_pixels = np.where(CHI2 == 1)
    DISC_PX_MAP[CHI2_1_pixels[0], CHI2_1_pixels[1]] = 1   
    
    ## Subtracting the dark from the scientific image
    SCI = np.subtract(SCI , DARK_SCI) 
    
    for count,det_POS in enumerate (det_POSs):
        if int(FITS_IMAGE[0].header['SCE_POS']) == det_POS:
        #  Setting the value of the signal to 0 ADU for the discarded pixels
            SCI = SCI * (1 - DISC_PX_MAP) * (1 - BAD_PIX_SCI[count,:,:]) 
            break
    return SCI,PID  
    
def search_for_PSF(SCI,file_name,min_surrounding_ADU, amount_max_search, min_signal_seed_px, max_surrounding_fail, count_seed_pixel_all,alarms_Nine_pixels, alarmsIDs, alarmsFile, alarms_x, alarms_y,alarmsPID,PID):
    #global min_surrounding_ADU, amount_max_search, min_signal_seed_px, max_surrounding_fail, count_seed_pixel_all
    start = time.time()       
    count_seed_pixel     = 0
    
    print('----------------------ID--------------------',file_name)   
        
    ## Initiliazing the while loop traffic light and amount of iteration
    count_max_loop = 0
    
    while count_max_loop < amount_max_search :
        # Initializing the count of the pixels surrounding the seed pixels below min_surrounding_ADU
        count_surrounding    = 0
        
        # Identifying the max value of the image in an iterative process
        Max_flux_px    =  np.where(SCI == np.max(SCI))
        
        # incrementing the amount of maxima evaluated
        count_max_loop       += 1 
        
        if Max_flux_px[0][0] > 0 and Max_flux_px[0][0] < 2039 and Max_flux_px[1][0] > 0 and Max_flux_px[1][0] <2039:
            maximum_and_surrounding_ADUs = SCI[Max_flux_px[0][0]-1:Max_flux_px[0][0]+2,Max_flux_px[1][0]-1:Max_flux_px[1][0]+2].ravel()
            #
            if maximum_and_surrounding_ADUs[4] > min_signal_seed_px:
                for maxima_and_surrounding_ADU in maximum_and_surrounding_ADUs:
                    if maxima_and_surrounding_ADU < min_surrounding_ADU:
                        count_surrounding += 1
                if count_surrounding > max_surrounding_fail + 1 :
                    #print(count_surrounding,'NOT OK This is not kept','pix',Max_flux_px[0][0],Max_flux_px[1][0])
                    #print(maximum_and_surrounding_ADUs)
                    SCI[Max_flux_px[0][0],Max_flux_px[1][0]] = 0  # This set to zero the max flux that has not been associated to the PSF
                else:
                    print(count_surrounding,'OK This is kept','pix',Max_flux_px[0][0],Max_flux_px[1][0])
                    print(maximum_and_surrounding_ADUs)
                    #print('000','pix',Max_flux_px[0][0],Max_flux_px[1][0],maximum_and_surrounding_ADUs) 

                    alarms_Nine_pixels.append(maximum_and_surrounding_ADUs)
                    alarmsIDs.append(count_seed_pixel)
                    alarmsFile.append(str(str(file_name)[1:-1])[1:-1].replace("'",""))
                    alarms_x.append(Max_flux_px[0][0])
                    alarms_y.append(Max_flux_px[1][0])
                    alarmsPID.append(PID)
                    
                    # This is to avoid multiple counting of the same cluster identifed as being a PSF
                    SCI[Max_flux_px[0][0]-1:Max_flux_px[0][0]+2,Max_flux_px[1][0]-1:Max_flux_px[1][0]+2] = 0
                    
                    # counter maxima attributed to potential PSF per file and acculumated
                    count_seed_pixel     += 1
                    count_seed_pixel_all += 1  
            else:
                break
    end = time.time()
    print('Time of execution',end - start) 
    return alarms_Nine_pixels, alarmsIDs, alarmsFile, alarms_x, alarms_y,alarmsPID, count_seed_pixel_all

def Create_cat_alarms(alarms_Nine_pixels, alarmsIDs, alarmsFile, alarms_x, alarms_y,alarmsPID, count_seed_pixel_all,seconds):
    today = date.today()
    alarms_Nine_pixels = np.array(alarms_Nine_pixels).reshape(count_seed_pixel_all,9)         
    CAT_ALARMS   = np.concatenate((alarms_Nine_pixels,
                                    np.array(alarmsIDs)[:,None],
                                    np.array(alarms_x)[:,None],np.array(alarms_y)[:,None],np.array(alarmsPID)[:,None]),axis=1)
    np.savetxt('Output/alarmsFile_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds),alarmsFile,delimiter='',fmt="%s")                                
    np.savetxt('Output/CAT_ALARMS_{}_{}'.format(int(today.strftime("%d%m%Y")),seconds),CAT_ALARMS,delimiter=',')
    return CAT_ALARMS

def plot_seed_pixel_scatter(CAT_ALARMS,seconds):
    today = date.today()
    out_pdf = 'Output/Seed_pixel_scatter_{}_{}.pdf'.format(int(today.strftime("%d%m%Y")),seconds)
    pdf = matplotlib.backends.backend_pdf.PdfPages(out_pdf)
    
    fig = plt.figure(figsize=(15,15))
    fig.subplots_adjust(hspace=0, wspace=0) 
    # Calculate the point density
    xy = np.vstack([CAT_ALARMS[:,-3],CAT_ALARMS[:,-2]])
    z = gaussian_kde(xy)(xy)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(CAT_ALARMS[:,-3],CAT_ALARMS[:,-2],c=z, s=100)
    ax.set_xlabel('X position seed pixel',fontsize=18)
    ax.set_ylabel('Y position',fontsize=18) 
    pdf.savefig()
    pdf.close()                               
    #column_names = ['FileID','Position in mm', 'Count alarms', 'X position', 'Y position','1','2','3','seed pixel (4)','5','6','7','8','9']

def get_param_from_fits_file_BIS(HDUL):
    MACCGRP                 = HDUL[0].header['GRP_CNT']
    MACCFR                  = HDUL[0].header['FRAMEGRP']
    MACCDR                  = HDUL[0].header['DROPS']
    DATE_OBS_sec_CET        = time.mktime(datetime.strptime(HDU1[0].header['DATE-OBS'], '%Y-%m-%dT%H:%M:%S').timetuple()) + 3600  
    return MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET

def study_channels(SCI_ALL, DET_ID, MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET):

    for count_det,det_POS in enumerate(det_POSs):
        if DET_ID == det_POS:
            for count_channel in range (32):
                Canali[count_det,int(count_canali[j]),0] = (SCI_ALL[count_channel*64:(count_channel+1)*64,:]-1024)/((MACCGRP-1)*(MACCFR+MACCDR))
                Canali[j,int(count_canali[j]),1] = time.mktime(datetime.strptime(HDU1[0].header['DATE-OBS'], '%Y-%m-%dT%H:%M:%S').timetuple())+3600
                Canali[j,int(count_canali[j]),2] = cc
                Canali[j,int(count_canali[j]),3] = np.median(HDU1['CHI2'].data[cc*64:(cc+1)*64,:])
                count_canali[j] += 1

def get_HK_param(path_files_HK, files_list_HK, t_min, t_max, param_HK_list_user):
    
    files_list_HK           = os.listdir(path_files_HK)
    shape_list_HK           = (len(files_list_HK))
    print('files_list_HK',files_list_HK)
    
    print('number of files HKTM',shape_list_HK)
    
    shape_list_HK           = (len(files_list_HK))
    an_array = 3,3,3

    CATALOG_HK_PARAM        = np.empty((60000,len(param_HK_list_user) + 1))
    CATALOG_HK_PARAM[:]     = np.NaN
    
    traffic_count           = 0
    i = 0 
    while i < shape_list_HK:
        if not fnmatch.fnmatch(files_list_HK[i], '*001_000*') and not fnmatch.fnmatch(files_list_HK[i], '*002_000*') and not fnmatch.fnmatch(files_list_HK[i], '*003_000*') and not fnmatch.fnmatch(files_list_HK[i], '*004_000*'):
            print(files_list_HK[i])
            files_list_HK   = np.delete(files_list_HK,i,0)
        else:
            print(path_files_HK + files_list_HK[i])  
            #if os.path.isfile(path_files_HK + files_list_HK[i]):
            HDUL_HK     = fits.open(path_files_HK + files_list_HK[i])
            Time_COARS  = HDUL_HK[1].data.OBTCOARS
            traffic_check = 0
            for count_param, param in enumerate(param_HK_list_user):
                try:
                    PARAM_COL = HDUL_HK[1].data['CALST{}'.format(param)]
                    if traffic_check == 1: 
                        for j in range (len(HDUL_HK[1].data)):
                            if t_min < Time_COARS[j] < t_max:
                                check_place = CATALOG_HK_PARAM[:,0].index(Time_COARS[j])
                                CATALOG_HK_PARAM[check_place,count_param + 1] = PARAM_COL[j]
                    else:      
                        for j in range (len(HDUL_HK[1].data)):
                            if t_min < Time_COARS[j] < t_max:          
                                CATALOG_HK_PARAM[traffic_count,count_param + 1] = PARAM_COL[j]
                                CATALOG_HK_PARAM[traffic_count,0] = Time_COARS[j]
                                traffic_check = 1
                                traffic_count += 1 
                except:
                    Except = True
            i += 1
        shape_list_HK  = (len(files_list_HK))
    print('traffic_count',traffic_count)                     
    return CATALOG_HK_PARAM
                                        
def create_catalog_IMAGES_CHANNELS(SCI_ALL,CHI2_ALL, DET_ID, MACCGRP, MACCFR, MACCDR, DATE_OBS_sec_CET,CATALOG_INFO_IMAGES_CHANNELS,PID,det_POSs,count_file_bis):   
    param_per_file_channels = []
    #for count_det,det_POS in enumerate(det_POSs):
    #    if DET_ID == det_POS:
    param_per_file_channels.append(PID)
    param_per_file_channels.append(DATE_OBS_sec_CET)
    param_per_file_channels.append(DET_ID)
    param_per_file_channels.append((MACCGRP-1)*(MACCFR+MACCDR))
    
    for count_channel in range (32):
        if count_channel == 0 :
            param_per_file_channels.append(np.nanmedian((SCI_ALL[4:(count_channel+1)*64,4:-4])))
        elif count_channel == 31 :
            param_per_file_channels.append(np.nanmedian((SCI_ALL[count_channel*64:-4,4:-4])))
        else:
            param_per_file_channels.append(np.nanmedian((SCI_ALL[count_channel*64:(count_channel+1)*64,4:-4])))
            
    for count_channel in range (32):  
        if count_channel == 0 :
            param_per_file_channels.append(np.nanmedian((CHI2_ALL[4:(count_channel+1)*64,4:-4])))
        elif count_channel == 31 :
            param_per_file_channels.append(np.nanmedian((CHI2_ALL[count_channel*64:-4,4:-4])))
        else:
            param_per_file_channels.append(np.nanmedian((CHI2_ALL[count_channel*64:(count_channel+1)*64,4:-4])))
    
    if count_file_bis == 0 :  
        CATALOG_INFO_IMAGES_CHANNELS = np.array(param_per_file_channels)[None,:]
    else:
        CATALOG_INFO_IMAGES_CHANNELS = np.concatenate((CATALOG_INFO_IMAGES_CHANNELS,np.array(param_per_file_channels)[None,:]))             
    
    return CATALOG_INFO_IMAGES_CHANNELS

def get_catalog_from_fits_RAWLINES(RW_FrameID, RW_RawlineID, RW_Rawline, DET_ID, DATE_OBS_sec_CET,CATALOG_INFO_RAWLINES,MACCGRP,MACCFR,MACCDR,ax,det_POSs):
    
    Ref_pix_2                    = np.zeros((MACCGRP*MACCFR,2048))
    Ref_pix_511                  = np.zeros((MACCGRP*MACCFR,2048))
    count_2                      = 0
    count_511                    = 0    
    time_plot,time_plot_median   = get_time_axis_RL(MACCGRP,MACCFR,MACCDR)
    j = 0
    while j < RW_FrameID.shape[0]:
        if RW_RawlineID[j] == 2:
            Ref_pix_2[count_2,:] = RW_Rawline[j]
            count_2 += 1

        if RW_RawlineID[j] == 511:
            Ref_pix_511[count_511,:] = RW_Rawline[j]
            count_511 += 1
        j += 1
    dict_det_POSs_color = {'11':'red', '12':'blue','13':'yellow','14':'green','21':'brown','22':'khaki','23':'firebrick','24':'olive','31':'magenta','32':'cyan','33':'turquoise','34':'forestgreen','41':'darkviolet','42':'darkgoldenrod','43':'orchid','44':'lime'}
    for count_pix, pix in enumerate([300,400,500,600,700,800,900]):
        for det_POS in (det_POSs):
            if DET_ID == det_POS :
                ax[2*(count_pix)].scatter(time_plot+DATE_OBS_sec_CET,Ref_pix_2[:,pix], marker = '+',
                                          c = dict_det_POSs_color['{}'.format(DET_ID)], label ='Detector {}'.format(DET_ID))
                ax[2*(count_pix)+1].scatter(time_plot+DATE_OBS_sec_CET,Ref_pix_511[:,pix], marker = '+',
                                          c = dict_det_POSs_color['{}'.format(DET_ID)], label ='Detector {}'.format(DET_ID))
                median1 =([])
        for h in range (MACCGRP):
            median1 = np.append(median1,np.median(Ref_pix_2[h*16:16*(h+1),pix]))
        pars, cov = curve_fit(f=linear1Dinterp, xdata=time_plot_median+DATE_OBS_sec_CET, ydata=median1, p0=[4000,0])
        ax[2*(count_pix)].plot(time_plot_median + DATE_OBS_sec_CET, linear1Dinterp(time_plot_median+DATE_OBS_sec_CET, *pars),'k-', linewidth=2)

        median1 =([])
        for h in range (MACCGRP):
            median1 = np.append(median1,np.median(Ref_pix_511[h*16:16*(h+1),pix]))
        pars2, cov2 = curve_fit(f=linear1Dinterp, xdata=time_plot_median+DATE_OBS_sec_CET, ydata=median1, p0=[5000,0.1])
        ax[2*(count_pix)+1].plot(time_plot_median + DATE_OBS_sec_CET, linear1Dinterp(time_plot_median+DATE_OBS_sec_CET, *pars2),'k-', linewidth=2)



def CATALOG_INFO_IMAGES_CHANNELS_prep(CATALOG_INFO_IMAGES_CHANNELS,det_POSs):
    count_per_det                         = np.zeros(16)
    CATALOG_INFO_IMAGES_CHANNELS_shape    = CATALOG_INFO_IMAGES_CHANNELS.shape[0]
    CATALOG_INFO_IMAGES_CHANNELS_shapeCOL = CATALOG_INFO_IMAGES_CHANNELS.shape[1]
    CATALOG_INFO_IMAGES_CHANNELS_plot     = np.empty((16,CATALOG_INFO_IMAGES_CHANNELS_shape,CATALOG_INFO_IMAGES_CHANNELS_shapeCOL))
    
    CATALOG_INFO_IMAGES_CHANNELS_plot[:]  = np.NaN
    
    for i in range (CATALOG_INFO_IMAGES_CHANNELS_shape):
        for count, det_POS in enumerate(det_POSs):
            if int(CATALOG_INFO_IMAGES_CHANNELS[i,2]) == det_POS:
                for j in range (CATALOG_INFO_IMAGES_CHANNELS_shapeCOL):
                    CATALOG_INFO_IMAGES_CHANNELS_plot[int(count),int(count_per_det[count]),j]  = CATALOG_INFO_IMAGES_CHANNELS[i,j]
                count_per_det[count]+=1   
                
    for i in range (16):
        CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,:] = CATALOG_INFO_IMAGES_CHANNELS_plot[i,:,:][np.argsort(CATALOG_INFO_IMAGES_CHANNELS_plot[i,:, 1])]
        
    return CATALOG_INFO_IMAGES_CHANNELS_plot

def CATALOG_INFO_RAWLINES_prep(CATALOG_INFO_RAWLINES,det_POSs):
    count_per_det                         = np.zeros(16)
    CATALOG_INFO_RAWLINES_shape           = CATALOG_INFO_RAWLINES.shape[0]
    CATALOG_INFO_RAWLINES_shapeCOL        = CATALOG_INFO_RAWLINES.shape[1]
    CATALOG_INFO_RAWLINES_plot            = np.empty((16,CATALOG_INFO_RAWLINES_shape,CATALOG_INFO_RAWLINES_shapeCOL))
    
    CATALOG_INFO_RAWLINES_plot[:]         = np.NaN
    
    for i in range (CATALOG_INFO_RAWLINES_shape):
        for count, det_POS in enumerate(det_POSs):
            if int(CATALOG_INFO_RAWLINES[i,1]) == det_POS:
                for j in range (CATALOG_INFO_RAWLINES_shapeCOL):
                    CATALOG_INFO_RAWLINES_plot[int(count),int(count_per_det[count]),j]  = CATALOG_INFO_RAWLINES[i,j]
                count_per_det[count]+=1   
                
    for i in range (16):
        CATALOG_INFO_RAWLINES_plot[i,:,:] = CATALOG_INFO_RAWLINES_plot[i,:,:][np.argsort(CATALOG_INFO_RAWLINES_plot[i,:, 0])]
    
    
    return CATALOG_INFO_RAWLINES_plot

def get_all_HK_param():
    param_list = {'0775':'NISP_DPUAG_5V_CPU_Current',
                    '4871':'NISP_DPUBG_5V_CPU_Current',
                    '0774':'NISP_DPUAG_3_3V_CPU_Current',
                    '4870':'NISP_DPUBG_3_3V_CPU_Current',
                    '0794':'NISP_DPUAG_CPU_Temperature',
                    '4890':'NISP_DPUBG_CPU_Temperature',
                    '0795':'NISP_DPUAG_MainDCDC_Temperature',
                    '4891':'NISP_DPUBG_MainDCDC_Temperature',
                    '0796':'NISP_DPUAG_DBB_Temperature',
                    '4892':'NISP_DPUBG_DBB_Temperature',
                    '0916':'NISP_DPUAG_DCU1_VDDA_V',
                    '1044':'NISP_DPUAG_DCU2_VDDA_V',
                    '1172':'NISP_DPUAG_DCU3_VDDA_V',
                    '1300':'NISP_DPUAG_DCU4_VDDA_V',
                    '1428':'NISP_DPUAG_DCU5_VDDA_V',
                    '1556':'NISP_DPUAG_DCU6_VDDA_V',
                    '1684':'NISP_DPUAG_DCU7_VDDA_V',
                    '1812':'NISP_DPUAG_DCU8_VDDA_V',
                    '5012':'NISP_DPUBG_DCU1_VDDA_V',
                    '5140':'NISP_DPUBG_DCU2_VDDA_V',
                    '5268':'NISP_DPUBG_DCU3_VDDA_V',
                    '5396':'NISP_DPUBG_DCU4_VDDA_V',
                    '5524':'NISP_DPUBG_DCU5_VDDA_V',
                    '5652':'NISP_DPUBG_DCU6_VDDA_V',
                    '5780':'NISP_DPUBG_DCU7_VDDA_V',
                    '5908':'NISP_DPUBG_DCU8_VDDA_V',
                    '0922':'NISP_DPUAG_DC1_VDDA_Rtrn',
                    '1050':'NISP_DPUAG_DC2_VDDA_Rtrn',
                    '1178':'NISP_DPUAG_DC3_VDDA_Rtrn',
                    '1306':'NISP_DPUAG_DC4_VDDA_Rtrn',
                    '1434':'NISP_DPUAG_DC5_VDDA_Rtrn',
                    '1562':'NISP_DPUAG_DC6_VDDA_Rtrn',
                    '1690':'NISP_DPUAG_DC7_VDDA_Rtrn',
                    '1818':'NISP_DPUAG_DC8_VDDA_Rtrn',
                    '5018':'NISP_DPUBG_DC1_VDDA_Rtrn',
                    '5146':'NISP_DPUBG_DC2_VDDA_Rtrn',
                    '5274':'NISP_DPUBG_DC3_VDDA_Rtrn',
                    '5402':'NISP_DPUBG_DC4_VDDA_Rtrn',
                    '5530':'NISP_DPUBG_DC5_VDDA_Rtrn',
                    '5658':'NISP_DPUBG_DC6_VDDA_Rtrn',
                    '5786':'NISP_DPUBG_DC7_VDDA_Rtrn',
                    '5914':'NISP_DPUBG_DC8_VDDA_Rtrn',
                    '0918':'NISP_DPUAG_DCU1_VREF_V',
                    '1046':'NISP_DPUAG_DCU2_VREF_V',
                    '1174':'NISP_DPUAG_DCU3_VREF_V',
                    '1302':'NISP_DPUAG_DCU4_VREF_V',
                    '1430':'NISP_DPUAG_DCU5_VREF_V',
                    '1558':'NISP_DPUAG_DCU6_VREF_V',
                    '1686':'NISP_DPUAG_DCU7_VREF_V',
                    '1814':'NISP_DPUAG_DCU8_VREF_V',
                    '5014':'NISP_DPUBG_DCU1_VREF_V',
                    '5142':'NISP_DPUBG_DCU2_VREF_V',
                    '5270':'NISP_DPUBG_DCU3_VREF_V',
                    '5398':'NISP_DPUBG_DCU4_VREF_V',
                    '5526':'NISP_DPUBG_DCU5_VREF_V',
                    '5654':'NISP_DPUBG_DCU6_VREF_V',
                    '5782':'NISP_DPUBG_DCU7_VREF_V',
                    '5910':'NISP_DPUBG_DCU8_VREF_V',
                    '0909':'NISP_DPUAG_DC1_V2P5D_V',
                    '1037':'NISP_DPUAG_DC2_V2P5D_V',
                    '1165':'NISP_DPUAG_DC3_V2P5D_V',
                    '1293':'NISP_DPUAG_DC4_V2P5D_V',
                    '1421':'NISP_DPUAG_DC5_V2P5D_V',
                    '1549':'NISP_DPUAG_DC6_V2P5D_V',
                    '1677':'NISP_DPUAG_DC7_V2P5D_V',
                    '1805':'NISP_DPUAG_DC8_V2P5D_V',
                    '5005':'NISP_DPUBG_DC1_V2P5D_V',
                    '5133':'NISP_DPUBG_DC2_V2P5D_V',
                    '5261':'NISP_DPUBG_DC3_V2P5D_V',
                    '5389':'NISP_DPUBG_DC4_V2P5D_V',
                    '5517':'NISP_DPUBG_DC5_V2P5D_V',
                    '5645':'NISP_DPUBG_DC6_V2P5D_V',
                    '5773':'NISP_DPUBG_DC7_V2P5D_V',
                    '5901':'NISP_DPUBG_DC8_V2P5D_V',
                    '0908':'NISP_DPUAG_DC1_V3P3D_V',
                    '1036':'NISP_DPUAG_DC2_V3P3D_V',
                    '1164':'NISP_DPUAG_DC3_V3P3D_V',
                    '1293':'NISP_DPUAG_DC4_V3P3D_V',
                    '1420':'NISP_DPUAG_DC5_V3P3D_V',
                    '1548':'NISP_DPUAG_DC6_V3P3D_V',
                    '1676':'NISP_DPUAG_DC7_V3P3D_V',
                    '1804':'NISP_DPUAG_DC8_V3P3D_V',
                    '5004':'NISP_DPUBG_DC1_V3P3D_V',
                    '5132':'NISP_DPUBG_DC2_V3P3D_V',
                    '5260':'NISP_DPUBG_DC3_V3P3D_V',
                    '5388':'NISP_DPUBG_DC4_V3P3D_V',
                    '5516':'NISP_DPUBG_DC5_V3P3D_V',
                    '5644':'NISP_DPUBG_DC6_V3P3D_V',
                    '5772':'NISP_DPUBG_DC7_V3P3D_V',
                    '5900':'NISP_DPUBG_DC8_V3P3D_V',
                    '0924':'NISP_DPUAG_DC1_SCE_Power',
                    '1052':'NISP_DPUAG_DC2_SCE_Power',
                    '1180':'NISP_DPUAG_DC3_SCE_Power',
                    '1308':'NISP_DPUAG_DC4_SCE_Power',
                    '1436':'NISP_DPUAG_DC5_SCE_Power',
                    '1564':'NISP_DPUAG_DC6_SCE_Power',
                    '1692':'NISP_DPUAG_DC7_SCE_Power',
                    '1820':'NISP_DPUAG_DC8_SCE_Power',
                    '5020':'NISP_DPUBG_DC1_SCE_Power',
                    '5148':'NISP_DPUBG_DC2_SCE_Power',
                    '5276':'NISP_DPUBG_DC3_SCE_Power',
                    '5404':'NISP_DPUBG_DC4_SCE_Power',
                    '5532':'NISP_DPUBG_DC5_SCE_Power',
                    '5660':'NISP_DPUBG_DC6_SCE_Power',
                    '5788':'NISP_DPUBG_DC7_SCE_Power',
                    '5916':'NISP_DPUBG_DC8_SCE_Power',
                    '0917':'NISP_DPUAG_DC1_VDDA_I',
                    '1045':'NISP_DPUAG_DC2_VDDA_I',
                    '1173':'NISP_DPUAG_DC3_VDDA_I',
                    '1301':'NISP_DPUAG_DC4_VDDA_I',
                    '1429':'NISP_DPUAG_DC5_VDDA_I',
                    '1557':'NISP_DPUAG_DC6_VDDA_I',
                    '1685':'NISP_DPUAG_DC7_VDDA_I',
                    '1813':'NISP_DPUAG_DC8_VDDA_I',
                    '5013':'NISP_DPUBG_DC1_VDDA_I',
                    '5141':'NISP_DPUBG_DC2_VDDA_I',
                    '5269':'NISP_DPUBG_DC3_VDDA_I',
                    '5397':'NISP_DPUBG_DC4_VDDA_I',
                    '5525':'NISP_DPUBG_DC5_VDDA_I',
                    '5653':'NISP_DPUBG_DC6_VDDA_I',
                    '5781':'NISP_DPUBG_DC7_VDDA_I',
                    '5909':'NISP_DPUBG_DC8_VDDA_I',
                    '0919':'NISP_DPUAG_DC1_VREF_I',
                    '1047':'NISP_DPUAG_DC2_VREF_I',
                    '1175':'NISP_DPUAG_DC3_VREF_I',
                    '1303':'NISP_DPUAG_DC4_VREF_I',
                    '1431':'NISP_DPUAG_DC5_VREF_I',
                    '1559':'NISP_DPUAG_DC6_VREF_I',
                    '1687':'NISP_DPUAG_DC7_VREF_I',
                    '1815':'NISP_DPUAG_DC8_VREF_I',
                    '5015':'NISP_DPUBG_DC1_VREF_I',
                    '5143':'NISP_DPUBG_DC2_VREF_I',
                    '5271':'NISP_DPUBG_DC3_VREF_I',
                    '5399':'NISP_DPUBG_DC4_VREF_I',
                    '5527':'NISP_DPUBG_DC5_VREF_I',
                    '5655':'NISP_DPUBG_DC6_VREF_I',
                    '5783':'NISP_DPUBG_DC7_VREF_I',
                    '5911':'NISP_DPUBG_DC8_VREF_I',
                    '0912':'NISP_DPUAG_DC1_V3P3_I',
                    '1040':'NISP_DPUAG_DC2_V3P3_I',
                    '1168':'NISP_DPUAG_DC3_V3P3_I',
                    '1296':'NISP_DPUAG_DC4_V3P3_I',
                    '1424':'NISP_DPUAG_DC5_V3P3_I',
                    '1552':'NISP_DPUAG_DC6_V3P3_I',
                    '1680':'NISP_DPUAG_DC7_V3P3_I',
                    '1808':'NISP_DPUAG_DC8_V3P3_I',
                    '5008':'NISP_DPUBG_DC1_V3P3_I',
                    '5136':'NISP_DPUBG_DC2_V3P3_I',
                    '5264':'NISP_DPUBG_DC3_V3P3_I',
                    '5392':'NISP_DPUBG_DC4_V3P3_I',
                    '5520':'NISP_DPUBG_DC5_V3P3_I',
                    '5648':'NISP_DPUBG_DC6_V3P3_I',
                    '5776':'NISP_DPUBG_DC7_V3P3_I',
                    '5904':'NISP_DPUBG_DC8_V3P3_I',
                    '0913':'NISP_DPUA_DC1_V2P5D_I',
                    '1041':'NISP_DPUA_DC2_V2P5D_I',
                    '1169':'NISP_DPUA_DC3_V2P5D_I',
                    '1297':'NISP_DPUA_DC4_V2P5D_I',
                    '1425':'NISP_DPUA_DC5_V2P5D_I',
                    '1553':'NISP_DPUA_DC6_V2P5D_I',
                    '1681':'NISP_DPUA_DC7_V2P5D_I',
                    '1809':'NISP_DPUA_DC8_V2P5D_I',
                    '5009':'NISP_DPUB_DC1_V2P5D_I',
                    '5137':'NISP_DPUB_DC2_V2P5D_I',
                    '5265':'NISP_DPUB_DC3_V2P5D_I',
                    '5393':'NISP_DPUB_DC4_V2P5D_I',
                    '5521':'NISP_DPUB_DC5_V2P5D_I',
                    '5649':'NISP_DPUB_DC6_V2P5D_I',
                    '5777':'NISP_DPUB_DC7_V2P5D_I',
                    '5905':'NISP_DPUB_DC8_V2P5D_I',
                    '1940':'NISP_DPUAG_SC1_SidecarTS',
                    '2068':'NISP_DPUAG_SC2_SidecarTS',
                    '2196':'NISP_DPUAG_SC3_SidecarTS',
                    '2324':'NISP_DPUAG_SC4_SidecarTS',
                    '2452':'NISP_DPUAG_SC5_SidecarTS',
                    '2580':'NISP_DPUAG_SC6_SidecarTS',
                    '2708':'NISP_DPUAG_SC7_SidecarTS',
                    '2836':'NISP_DPUAG_SC8_SidecarTS',
                    '6036':'NISP_DPUBG_SC1_SidecarTS',
                    '6164':'NISP_DPUBG_SC2_SidecarTS',
                    '6292':'NISP_DPUBG_SC3_SidecarTS',
                    '6420':'NISP_DPUBG_SC4_SidecarTS',
                    '6548':'NISP_DPUBG_SC5_SidecarTS',
                    '6676':'NISP_DPUBG_SC6_SidecarTS',
                    '6804':'NISP_DPUBG_SC7_SidecarTS',
                    '6932':'NISP_DPUBG_SC8_SidecarTS',
                    '1928':'NISP_DPUAG_SC1_VDDA_Vtge',
                    '2056':'NISP_DPUAG_SC2_VDDA_Vtge',
                    '2184':'NISP_DPUAG_SC3_VDDA_Vtge',
                    '2312':'NISP_DPUAG_SC4_VDDA_Vtge',
                    '2440':'NISP_DPUAG_SC5_VDDA_Vtge',
                    '2568':'NISP_DPUAG_SC6_VDDA_Vtge',
                    '2696':'NISP_DPUAG_SC7_VDDA_Vtge',
                    '2824':'NISP_DPUAG_SC8_VDDA_Vtge',
                    '6024':'NISP_DPUBG_SC1_VDDA_Vtge',
                    '6152':'NISP_DPUBG_SC2_VDDA_Vtge',
                    '6280':'NISP_DPUBG_SC3_VDDA_Vtge',
                    '6408':'NISP_DPUBG_SC4_VDDA_Vtge',
                    '6536':'NISP_DPUBG_SC5_VDDA_Vtge',
                    '6664':'NISP_DPUBG_SC6_VDDA_Vtge',
                    '6792':'NISP_DPUBG_SC7_VDDA_Vtge',
                    '6920':'NISP_DPUBG_SC8_VDDA_Vtge',
                    '1922':'NISP_DPUAG_SC1_VResetVtg',
                    '2050':'NISP_DPUAG_SC2_VResetVtg',
                    '2178':'NISP_DPUAG_SC3_VResetVtg',
                    '2306':'NISP_DPUAG_SC4_VResetVtg',
                    '2434':'NISP_DPUAG_SC5_VResetVtg',
                    '2562':'NISP_DPUAG_SC6_VResetVtg',
                    '2690':'NISP_DPUAG_SC7_VResetVtg',
                    '2818':'NISP_DPUAG_SC8_VResetVtg',
                    '6018':'NISP_DPUBG_SC1_VResetVtg',
                    '6146':'NISP_DPUBG_SC2_VResetVtg',
                    '6724':'NISP_DPUBG_SC3_VResetVtg',
                    '6402':'NISP_DPUBG_SC4_VResetVtg',
                    '6530':'NISP_DPUBG_SC5_VResetVtg',
                    '6658':'NISP_DPUBG_SC6_VResetVtg',
                    '6786':'NISP_DPUBG_SC7_VResetVtg',
                    '6914':'NISP_DPUBG_SC8_VResetVtg',
                    '1923':'NISP_DPUA_SC1_DSubVtge',
                    '2051':'NISP_DPUA_SC2_DSubVtge',
                    '2179':'NISP_DPUA_SC3_DSubVtge',
                    '2307':'NISP_DPUA_SC4_DSubVtge',
                    '2435':'NISP_DPUA_SC5_DSubVtge',
                    '2563':'NISP_DPUA_SC6_DSubVtge',
                    '2691':'NISP_DPUA_SC7_DSubVtge',
                    '2819':'NISP_DPUA_SC8_DSubVtge',
                    '6019':'NISP_DPUB_SC1_DSubVtge',
                    '6147':'NISP_DPUB_SC2_DSubVtge',
                    '6275':'NISP_DPUB_SC3_DSubVtge',
                    '6403':'NISP_DPUB_SC4_DSubVtge',
                    '6531':'NISP_DPUB_SC5_DSubVtge',
                    '6659':'NISP_DPUB_SC6_DSubVtge',
                    '6787':'NISP_DPUB_SC7_DSubVtge',
                    '6915':'NISP_DPUB_SC8_DSubVtge',
                    '1938':'NISP_DPUAG_SC1_H2TS33int',
                    '2066':'NISP_DPUAG_SC2_H2TS33int',
                    '2194':'NISP_DPUAG_SC3_H2TS33int',
                    '2322':'NISP_DPUAG_SC4_H2TS33int',
                    '2450':'NISP_DPUAG_SC5_H2TS33int',
                    '2578':'NISP_DPUAG_SC6_H2TS33int',
                    '2706':'NISP_DPUAG_SC7_H2TS33int',
                    '2834':'NISP_DPUAG_SC8_H2TS33int',
                    '6034':'NISP_DPUBG_SC1_H2TS33int',
                    '6162':'NISP_DPUBG_SC2_H2TS33int',
                    '6290':'NISP_DPUBG_SC3_H2TS33int',
                    '6418':'NISP_DPUBG_SC4_H2TS33int',
                    '6546':'NISP_DPUBG_SC5_H2TS33int',
                    '6674':'NISP_DPUBG_SC6_H2TS33int',
                    '6802':'NISP_DPUBG_SC7_H2TS33int',
                    '6930':'NISP_DPUBG_SC8_H2TS33int',
                    '0619':'NISP_OMATC_CSS1_CryoTemp CSS-cryo',
                    '0620':'NISP_OMATC_CSS2_CryoTemp CSS-cryo',
                    '0621':'NISP_OMATC_CSS1_CryoTemp CSS-cryo',
                    '0622':'NISP_OMATC_CSS2_CryoTemp CSS-cryo',
                    '0643':'NISP_DPUAG_LSQ_FitDeltaT',
                    '4739':'NISP_DPUBG_LSQ_FitDeltaT',
                    '0644':'NISP_DPUAG_Dat&X2_DeltaT',
                    '4740':'NISP_DPUBG_Dat&X2_DeltaT',
                    '0645':'NISP_DPUAG_DataComprFact',
                    '4741':'NISP_DPUBG_DataComprFact',
                    '0646':'NISP_DPUAG_Chi2ComprFact',
                    '4742':'NISP_DPUBG_Chi2ComprFact',
                    '3975':'NISP_DPUAG_DRBcorEdaErCt',
                    '8071':'NISP_DPUBG_DRBcorEdaErCt',
                    '3974':'NISP_DPUAG_DRBmltEdaErCt',
                    '8070':'NISP_DPUBG_DRBmltEdaErCt',
                    '3987':'NISP_DPUAG_DRB_SpW_ErrCt',
                    '8083':'NISP_DPUBG_DRB_SpW_ErrCt',
                    '3970':'NISP_DPUAG_DBBnCrScrErCt',
                    '8066':'NISP_DPUBG_DBBnCrScrErCt',
                    '3971':'NISP_DPUAG_DBBcorScrErCt',
                    '8067':'NISP_DPUBG_DBBcorScrErCt',
                    '3972':'NISP_DPUAG_DBBmltEdaErCt',
                    '8068':'NISP_DPUBG_DBBmltEdaErCt',
                    '3973':'NISP_DPUAG_DBBcorEdaErCt',
                    '8069':'NISP_DPUBG_DBBcorEdaErCt',
                    '3976':'NISP_DPUAG_DCU_SpW1_ErCt',
                    '3977':'NISP_DPUAG_DCU_SpW2_ErCt',
                    '3978':'NISP_DPUAG_DCU_SpW3_ErCt',
                    '3979':'NISP_DPUAG_DCU_SpW4_ErCt',
                    '3980':'NISP_DPUAG_DCU_SpW5_ErCt',
                    '3981':'NISP_DPUAG_DCU_SpW6_ErCt',
                    '3982':'NISP_DPUAG_DCU_SpW7_ErCt',
                    '3983':'NISP_DPUAG_DCU_SpW8_ErCt',
                    '8072':'NISP_DPUBG_DCU_SpW1_ErCt',
                    '8073':'NISP_DPUBG_DCU_SpW2_ErCt',
                    '8074':'NISP_DPUBG_DCU_SpW3_ErCt',
                    '8075':'NISP_DPUBG_DCU_SpW4_ErCt',
                    '8076':'NISP_DPUBG_DCU_SpW5_ErCt',
                    '8077':'NISP_DPUBG_DCU_SpW6_ErCt',
                    '8078':'NISP_DPUBG_DCU_SpW7_ErCt',
                    '8079':'NISP_DPUBG_DCU_SpW8_ErCt',
                    '0620':'NISP_OMATC_CSS2_CryoTemp',
                    '0621':'NISP_OMATC_SSS1_CryoTemp',
                    '0622':'NISP_OMATC_SSS2_CryoTemp',
                    '0623':'NISP_OMATC_FWA_CryoTemp',
                    '0624':'NISP_OMATC_GWA_CryoTemp',
                    '0625':'NISP_OMATC_SA1_FRngTemp',
                    '0626':'NISP_OMATC_SA2_FRngTemp',
                    '0627':'NISP_OMATC_SA3_FRngTemp',
                    '0628':'NISP_OMATC_SA4_FRngTemp',
                    '0629':'NISP_OMATC_SA5_FRngTemp',
                    '0630':'NISP_OMATC_SA6_FRngTemp',
                    '0631':'NISP_OMATC_CSS1_FRngTemp',
                    '0632':'NISP_OMATC_CSS2_FRngTemp',
                    '0633':'NISP_OMATC_SSS1_FRngTemp',
                    '0634':'NISP_OMATC_SSS2_FRngTemp',
                    '0635':'NISP_OMATC_FWA_FRngTemp',
                    '0636':'NISP_OMATC_GWA_FRngTemp',
                    '0102':'ICU_LVPS_Temp',
                    '0101':'ICU-CDPU_Temp',
                    '0103':'ICU 5V Voltage',
                    '0104':'ICU 5V Current',
                    '0485':'OMACU_LED_1_Voltage',
                    '0486':'OMACU_LED_2_Voltage',
                    '0487':'OMACU_LED_3_Voltage',
                    '0488':'OMACU_LED_4_Voltage',
                    '0489':'OMACU_LED_5_Voltage'}
    
    return param_list