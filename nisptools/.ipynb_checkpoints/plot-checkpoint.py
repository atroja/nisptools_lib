import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from nisptools.detectors import Detectors

def plot_focalplane (det_frame, cmap='viridis', aspect='auto', scale='LOG', vmin = 1e-5, vmax = 1., title="NISP Focal Plane", savefig=False, outname = "NONE", interp='antialiased'):
    
    det = Detectors ()
    
    fig, axes = plt.subplots(nrows=4, ncols=4, figsize = (15,15), gridspec_kw={'wspace':0.05})
    
    fig.suptitle(title, fontsize=16)
    
    if scale == 'LIN':
        for i in range (1,3):
            for j in range (1,5):
                im = axes[i-1][j-1].imshow (det_frame [det.det2indx[str(i)+str(j)]-1], cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
        for i in range (3,5):
            for j in range (1,5):
                im = axes[i-1][j-1].imshow (np.rot90(det_frame [det.det2indx[str(i)+str(j)]-1],2), cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)

    
    if scale == 'LOG':
        for i in range (1,3):
            for j in range (1,5):
                im = axes[i-1][j-1].imshow (det_frame [det.det2indx[str(i)+str(j)]-1], norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
        for i in range (3,5):
            for j in range (1,5):
                im = axes[i-1][j-1].imshow (np.rot90(det_frame [det.det2indx[str(i)+str(j)]-1],2), norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
        
    for ax in axes.reshape (-1):
        ax.set_xticks([])
        ax.set_yticks([])
            
    #SIDE COLORBAR
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    
    for i in range (1,5):
        for j in range (1,5):
            axes[i-1][j-1].text(0.1, 0.8, str(i)+str(j))
            
    if savefig:
        plt.savefig (outname, bbox_inches='tight')
    
    plt.show ()
    
    
def plot_detector (det_frame, det_id = -1, extents = [-1,-1,-1,-1], cmap='viridis', aspect='auto', scale='LOG', vmin = 1e-5, vmax = 1., title="NISP Focal Plane", savefig=False, outname = "NONE", interp='none'):

    det = Detectors ()
    
    fig = plt.figure(figsize = (10,10))
    
    fig.suptitle(title, fontsize=16)
    
    if det_id == -1:
        if extents [0] == -1:
            if scale == 'LIN':
                plt.imshow (det_frame, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
            if scale == 'LOG':        
                plt.imshow (det_frame, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
        else:
            row_i = extents [3]
            row_e = extents [2]
            col_i = extents [0]
            col_e = extents [1]
            if scale == 'LIN':
                plt.imshow (det_frame[row_i:row_e, col_i:col_e], extent=extents, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
            if scale == 'LOG':        
                plt.imshow (det_frame[row_i:row_e, col_i:col_e], extent=extents, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)

                    
    if det_id != -1:
        if extents [0] == -1:
        
            if scale == 'LIN':
                if int(det_id) < 31:
                    plt.imshow (det_frame [det.det2indx[det_id]-1], cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
                else:
                    plt.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2), cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
        
                
            if scale == 'LOG':        
                if int(det_id) < 31:
                    plt.imshow (det_frame [det.det2indx[det_id]-1], norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
                else:
                    plt.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2), norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
        else:
    
            row_i = extents [3]
            row_e = extents [2]
            col_i = extents [0]
            col_e = extents [1]
        
            if scale == 'LIN':
                if int(det_id) < 31:
                    plt.imshow (det_frame [det.det2indx[det_id]-1][row_i:row_e, col_i:col_e], extent=extents, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
                else:
                    plt.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2)[row_i:row_e, col_i:col_e], extent=extents, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
        
                
            if scale == 'LOG':        
                if int(det_id) < 31:
                    plt.imshow (det_frame [det.det2indx[det_id]-1][row_i:row_e, col_i:col_e], extent=extents, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
                else:
                    plt.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2)[row_i:row_e, col_i:col_e], extent=extents, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
        


    plt.colorbar ()

    if savefig:
        plt.savefig (outname, bbox_inches='tight')
    
    plt.show ()
    
def plot_detector_array (ax, det_frame, det_id = -1, extents = [-1,-1,-1,-1], cmap='viridis', aspect='auto', scale='LOG', vmin = 1e-5, vmax = 1., title="NISP Focal Plane", savefig=False, outname = "NONE", interp='none'):

    det = Detectors ()
    
    if det_id == -1:
        if extents [0] == -1:
            if scale == 'LIN':
                ax.imshow (det_frame, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
            if scale == 'LOG':        
                ax.imshow (det_frame, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
        else:
            row_i = extents [3]
            row_e = extents [2]
            col_i = extents [0]
            col_e = extents [1]
            if scale == 'LIN':
                ax.imshow (det_frame[row_i:row_e, col_i:col_e], extent=extents, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
            if scale == 'LOG':        
                ax.imshow (det_frame[row_i:row_e, col_i:col_e], extent=extents, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)

                    
    if det_id != -1:
        if extents [0] == -1:
        
            if scale == 'LIN':
                if int(det_id) < 31:
                    ax.imshow (det_frame [det.det2indx[det_id]-1], cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
                else:
                    ax.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2), cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
        
                
            if scale == 'LOG':        
                if int(det_id) < 31:
                    ax.imshow (det_frame [det.det2indx[det_id]-1], norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
                else:
                    ax.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2), norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
        else:
    
            row_i = extents [3]
            row_e = extents [2]
            col_i = extents [0]
            col_e = extents [1]
        
            if scale == 'LIN':
                if int(det_id) < 31:
                    ax.imshow (det_frame [det.det2indx[det_id]-1][row_i:row_e, col_i:col_e], extent=extents, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
                else:
                    ax.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2)[row_i:row_e, col_i:col_e], extent=extents, cmap=cmap, aspect=aspect, vmin = vmin, vmax = vmax, interpolation = interp)
        
                
            if scale == 'LOG':        
                if int(det_id) < 31:
                    ax.imshow (det_frame [det.det2indx[det_id]-1][row_i:row_e, col_i:col_e], extent=extents, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)
                else:
                    ax.imshow (np.rot90(det_frame [det.det2indx[det_id]-1],2)[row_i:row_e, col_i:col_e], extent=extents, norm=LogNorm(vmin = vmin, vmax = vmax), cmap=cmap, aspect=aspect, interpolation = interp)