import numpy as np
import time
from datetime import datetime

def mjd (strtime, daylight_savings = False):
    mjdtime = np.empty (np.shape(strtime), dtype = float)
    
    if daylight_savings:
        for i in range(len(strtime)):
            mjdtime [i] = time.mktime(datetime.strptime(strtime [i], '%Y-%m-%dT%H:%M:%S').timetuple())+7200
    else: 
        for i in range(len(strtime)):
            mjdtime [i] = time.mktime(datetime.strptime(strtime [i], '%Y-%m-%dT%H:%M:%S').timetuple())+3600
    
    return mjdtime