import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from astropy.io import fits

class Detectors:

    det_trip = {"11" : "453", "12" : "272", "13" : "632", "14" : "267", 
                "21" : "268", "22" : "285", "23" : "548", "24" : "452", 
                "31" : "280", "32" : "284", "33" : "278", "34" : "269", 
                "41" : "458", "42" : "249", "43" : "221", "44" : "628"}
    
    inv_det_trip = {value: key
                     for key, value in det_trip.items()}
    
    det_trip_int = {int (key): int(value)
                     for key, value in det_trip.items()}
    
    inv_det_trip_int = {int (value): int(key)
                     for key, value in det_trip.items()}
    
    indx2det = {1: '11', 2: '21', 3: '31', 4: '41',
                5: '12', 6: '22', 7: '32', 8: '42',
                9: '13', 10: '23', 11: '33', 12: '43',
                13: '14', 14: '24', 15: '34', 16: '44'}     
    
    det2indx = {value: key
                 for key, value in indx2det.items()}
    
    indx2det_int = {value: int(key)
                     for value, key in indx2det.items()}
    
    
class Channels (Detectors):
    
    def __init__ (self):
        self.channel_mask = np.zeros ([32,2048], dtype = bool)
        for i in range (32):
            for j in range (64):
                self.channel_mask [i][i*64+j] = True
        self.channel_mask [0][:4] = False
        self.channel_mask [31][-4:] = False
        
        
    def channelize (self, frame, NPIX = 2048, NCHANNELS = 32):
        
        frame_channel = np.zeros ([16, NPIX-8,NCHANNELS])
        for det in range (16):
            row_count = 0
            for row in range (NPIX):
                if row <= 4 or row >= 2045:
                    continue
                for chl in range (NCHANNELS):
                    good_pixel = np.where (frame [det][row,self.channel_mask[chl]] > 0)
                    frame_channel [det][row_count,chl] = np.mean(frame [det][row,self.channel_mask[chl]][good_pixel[0]])
                row_count += 1

        return frame_channel
    
    def channelize_1frame (self, frame, NPIX = 2048, NCHANNELS = 32):
        
        frame_channel = np.zeros ([NPIX-8,NCHANNELS])
        row_count = 0
        for row in range (NPIX):
            if row <= 4 or row >= 2045:
                continue
            for chl in range (NCHANNELS):
                good_pixel = np.where (frame [row,self.channel_mask[chl]] > 0)
                frame_channel [row_count,chl] = np.mean(frame [row,self.channel_mask[chl]][good_pixel[0]])
            row_count += 1

        return frame_channel


    def Channel_stats (self, frame_channel, NCHANNELS = 32):
        
        frame_channel_mean   = np.ones ([16,NCHANNELS])
        frame_channel_median = np.zeros ([16,NCHANNELS])
        
        for det in range (16):
            for chl in range (NCHANNELS):
                frame_channel_mean [det][chl] = np.mean (frame_channel [det][:][chl])
                frame_channel_median [det][chl] = np.median (frame_channel [det][:][chl])   
        return frame_channel_mean, frame_channel_median
    
    def Channel_stats_1frame (self, frame_channel, NCHANNELS = 32):
        
        frame_channel_mean   = np.ones (NCHANNELS)
        frame_channel_median = np.zeros (NCHANNELS)
        
        for chl in range (NCHANNELS):
            frame_channel_mean [chl] = np.mean (frame_channel [:][chl])
            frame_channel_median [chl] = np.median (frame_channel [:][chl])   
        return frame_channel_mean, frame_channel_median
    
class Frames (Detectors):
    
    def __init__ (self, frame_fname=None, frame=None, hduname = 'SCIENCE', datatype = int):
        
        if frame_fname and frame:
            print ("ERROR: You must declare only one between frame_fname and frame")
            return 0
        if frame_fname:
            tmp = fits.open (frame_fname)
            self.frame = tmp[hduname].data.astype (datatype)
            self.det = tmp[0].header ['SCE_POS']
            self.scs = tmp[0].header ['SCS_ID'][:3]
            tmp.close ()
        else:
            self.frame = frame

def ops_plot (frame1, frame2, frame3, plot_type = '3frames', 
              norm1 = 'LOG', norm2 = 'LOG', norm3 = 'LOG',
              vmin = 1., vmax = 65536,
              frame1_name = 'Frame 1', frame2_name = 'Frame 2', frame3_name = 'Frame 3',
              detector = 0, cut = 0):
    
    fig = plt.figure (figsize=(20,5))
    ax = fig.subplots (1, 3).reshape (-1)
    
    ax[0].set_title (frame1_name)
    if norm1 == 'LOG':
        img1 = ax[0].imshow (frame1, norm=LogNorm ())
    else:
        img1 = ax[0].imshow (frame1)
    fig.colorbar (img1, ax = ax[0])
    
    ax[1].set_title (frame2_name)
    if norm2 == 'LOG':
        img2 = ax[1].imshow (frame2, norm=LogNorm ())
    else:
        img2 = ax[1].imshow (frame2)
    fig.colorbar (img2, ax = ax[1])
    
    if plot_type == '3frames':
        ax[2].set_title ("Difference frame")
        if norm3 == 'LOG':
            img3 = ax[2].imshow (frame3, norm=LogNorm ())
        else:
            img3 = ax[2].imshow (frame3)
        fig.colorbar (img3, ax = ax[2])    
        
    if plot_type == '2frames1histo':
        ax[2].set_title ("Difference histogram")
        ax[2].set_xlabel ("Difference value per the pixel")
        ax[2].set_ylabel ("Occurencies")        

        ax[2].plot ((frame3 [1][1:]+frame3 [1][:-1])/2., frame3 [0], label = "DET {}".format(detector))
        if cut != 0:
            ax[2].axvline (cut, color = 'k', linestyle = "dashed", label = "1500 cut")

        if norm3 == 'LOG':
            ax[2].set_yscale ('log')
            
        ax [2].legend ()
        
def ops (frame1, frame2, ops = 'diff', histo = False, nbins = -1, cut = 0, plot = False, 
         frame1_name = "None", frame2_name = "None", detector = 0):
    
    if ops == 'diff':
        diff = frame1 - frame2
        
        if histo: 
            hist = np.histogram (diff, bins = nbins)   
        
            if cut:
                diff_line = diff[4:-4,4:-4].reshape (-1)
                diff_sort = np.sort (diff_line)
                indx = np.where (diff_sort < cut)
                
                npix_cut = (indx[0][-1])


        if plot:
            
            if histo:
                            
                ops_plot (frame1, frame2, hist, plot_type = '2frames1histo',
                          frame1_name = frame1_name, frame2_name = frame2_name,
                          frame3_name = "Difference frame",
                          detector = detector, cut = cut
                         )
            
            else:
                ops_plot (frame1, frame2, diff, plot_type = '3frames',
                          frame1_name = frame1_name, frame2_name = frame2_name,
                          frame3_name = "Difference histogram",
                         )


    if histo:
        if cut:
            return diff, histo, npix_cut
        else:
            return diff, histo
    else:
        return diff
    
def create_mask (array, parname='DETID', par=['11', '12', '13', '14', '21', '22', '23', '24', '31', '32', '33', '34', '41', '42', '43', '44', ]):
    DET = Detectors ()
    mask = np.zeros ([len(par), len (array[parname])], dtype = bool)
    cnt=0
    for k in array [parname]:
        for i in par:
            if k == i:
                mask [DET.det2indx[i]-1][cnt] = True
                break
        cnt+=1
    return mask 